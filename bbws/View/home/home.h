#pragma once
class Home {
public:
	const char* bootstrap_min_css();
	const char* bootstrap_min_js();
	const char* font_awesome_min_css();
	const char* index_html();
	const char* jquery_min_js();
	const char* logo_jpg();
	const char* popper_min_js();
	const char* fontawesome_webfont_woff2();
	const char* fontawesome_webfont_woff();
	const char* fontawesome_webfont_ttf();
};