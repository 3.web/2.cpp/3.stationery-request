#include "UserDb.h"

void UserDb::Init() {
    int rc;
    char* error;

    // Open Database
    //std::cout << "Opening upware.db ..." << std::endl;
    rc = sqlite3_open("UserDB.db", &db);
    if (rc)
    {
        std::cerr << "Error opening User database: " << sqlite3_errmsg(db) << std::endl << std::endl;
        sqlite3_close(db);
        return;
    }
    else
    {
        //std::cout << "Opened HexFile.db" << std::endl << std::endl;
    }

    // Execute SQL
    //std::cout << "Creating firmwareTb ..." << std::endl;
    const char* sqlCreateTable = "CREATE TABLE User (id INTEGER PRIMARY KEY, name STRING, department INTEGER, password STRING, email STRING, position INTEGER, sessionId STRING);";
    //const char* sqlCreateTable = "DROP TABLE User;";      // Delete table
    rc = sqlite3_exec(db, sqlCreateTable, NULL, NULL, &error);
    if (rc)
    {
        //std::cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << std::endl << std::endl;
        sqlite3_free(error);
        //return;
    }
    else
    {
        //std::cout << "Created firmwareTb." << std::endl << std::endl;
    }

    
    // Execute SQL
    //std::cout << "Inserting a value into firmwareTb ..." << std::endl;
    //const char* sqlInsert = "INSERT INTO User(name, password, email, position, sessionId) VALUES('Tran Thien Dong', '123', 'thiendong.tran@ideagroup.vn', 0, 'null');";
    ////const char* sqlInsert = "DELETE FROM firmwareTb WHERE id = 2;";
    //rc = sqlite3_exec(db, sqlInsert, NULL, NULL, &error);
    //if (rc)
    //{
    //    std::cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << std::endl << std::endl;
    //    sqlite3_free(error);
    //}
    //else
    //{
    //    std::cout << "Inserted a value into firmwareTb." << std::endl << std::endl;
    //}
    

    /*
        // Execute SQL
        std::cout << "Inserting a value into firmwareTb ..." << std::endl;
        const char* sqlInsert = "INSERT INTO firmwareTb(userId, firmwareId, name, platform, version, fileCount, downloadCount) VALUES('a6hcH017Q4diBZCmn1KKQcAvMgB2', '55b1f340-f328-427e-bf8a-038e16f55c8f', 'IOT Device 1', 'STM32F103ZE', 'V2.1.0', 25, 1257);";
        rc = sqlite3_exec(db, sqlInsert, NULL, NULL, &error);
        if (rc)
        {
            std::cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << std::endl << std::endl;
            sqlite3_free(error);
        }
        else
        {
            std::cout << "Inserted a value into MyTable." << std::endl << std::endl;
        }
    */

#ifdef DEBUG
    std::cout << "\r\nOpen User Database Successful" << std::endl;;
#endif

    // Display MyTable
    //std::cout << "Retrieving values in firmwareTb ..." << std::endl;
    const char* sqlSelect = "SELECT * FROM User;";
    char** results = NULL;
    int rows, columns;
    sqlite3_get_table(db, sqlSelect, &results, &rows, &columns, &error);
    // Display Table
    for (int rowCtr = 0; rowCtr <= rows; ++rowCtr)
    {
        for (int colCtr = 0; colCtr < columns; ++colCtr)
        {
            // Determine Cell Position
            int cellPosition = (rowCtr * columns) + colCtr;

            if ((colCtr == 1) || (colCtr == 3)) {
                // Display Cell Value
                std::cout.width(36);
                std::cout.setf(std::ios::left);
                std::cout << results[cellPosition] << " ";
            }
            else if (colCtr == 2) {
                // Display Cell Value
                std::cout.width(12);
                std::cout.setf(std::ios::left);
                std::cout << results[cellPosition] << " ";
            }
            else {
                // Display Cell Value
                std::cout.width(12);
                std::cout.setf(std::ios::left);
                std::cout << results[cellPosition] << " ";
            }
        }

        // End Line
        std::cout << std::endl;

        // Display Separator For Header
        if (0 == rowCtr)
        {
            for (int colCtr = 0; colCtr < columns; ++colCtr)
            {
                if ((colCtr == 1)|| (colCtr == 3)) {
                    std::cout.width(36);
                    std::cout.setf(std::ios::left);
                    std::cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ";
                }
                else if (colCtr == 2) {
                    std::cout.width(12);
                    std::cout.setf(std::ios::left);
                    std::cout << "~~~~~~~~~~~~ ";
                }
                else {
                    std::cout.width(12);
                    std::cout.setf(std::ios::left);
                    std::cout << "~~~~~~~~~~~~ ";
                } 

            }
            std::cout << std::endl;
        }
    }
    sqlite3_free_table(results);
    sqlite3_free(error);
}
void UserDb::Create(User user) {
    char* error;
    // Execute SQL
    //cout << "Inserting a value into firmwareTb ..." << endl;
    //const char* sqlInsert = "INSERT INTO firmwareTb(userId, firmwareId, name, platform, version, fileCount, downloadCount) VALUES('a6hcH017Q4diBZCmn1KKQcAvMgB2', '55b1f340-f328-427e-bf8a-038e16f55c8f', 'IOT Device 1', 'STM32F103ZE', 'V2.1.0', 25, 1257);";
    char sqlInsert[256];
    sprintf_s(sqlInsert, "INSERT INTO User(name, department, password, email, position, sessionId) VALUES('%s',%d,'%s','%s',%d,'null');",
        user.Name.c_str(),
        user.Department,
        user.Password.c_str(),
        user.Email.c_str(),
        (int)user.Position);
    int rc = sqlite3_exec(db, sqlInsert, NULL, NULL, &error);
    if (rc)
    {
        //cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << endl << endl;
        sqlite3_free(error);
    }
    else
    {
        //cout << "Inserted a value into firmwareTb." << endl << endl;
    }
}
std::string UserDb::GetAll() {
    char sqlSelect[128];
    sprintf_s(sqlSelect, "SELECT * FROM User;");
    //cout << sqlSelect;
    char** results = NULL;
    int rows, columns;
    char* error;
    sqlite3_get_table(db, sqlSelect, &results, &rows, &columns, &error);

    std::string jsonReturn = "[";
    for (int rowCtr = 1; rowCtr <= rows; ++rowCtr)
    {
        if (rowCtr != 1) {
            jsonReturn += ",";
        }

        int cellPosition = (rowCtr * columns) + 0;
        jsonReturn = jsonReturn + "{\"id\":" + results[cellPosition] + ",\"name\":\"" + results[cellPosition + 1] + "\",\"department\":" + results[cellPosition + 2] + ",\"password\":\"" + results[cellPosition + 3] + "\",\"email\":\"" + results[cellPosition + 4] + "\",\"position\":" + results[cellPosition + 5] + "}";

        // End Line
        std::cout << std::endl;
    }
    jsonReturn += "]";

    return jsonReturn;
}
bool UserDb::CheckExistWithSessionId(std::string sessionId) {
    char sqlSelect[128];
    sprintf_s(sqlSelect, "SELECT * FROM User WHERE sessionId='%s';", sessionId.c_str());
    //cout << sqlSelect;
    char** results = NULL;
    int rows, columns;
    char* error;
    sqlite3_get_table(db, sqlSelect, &results, &rows, &columns, &error);

    if (rows > 0)
        return true;
    else return false;
}
bool UserDb::CheckExistWithSessionId(std::string sessionId, std::string *name) {
    char sqlSelect[128];
    sprintf_s(sqlSelect, "SELECT * FROM User WHERE sessionId='%s';", sessionId.c_str());
    //cout << sqlSelect;
    char** results = NULL;
    int rows, columns;
    char* error;
    sqlite3_get_table(db, sqlSelect, &results, &rows, &columns, &error);
    
    *name = results[8];
    if (rows > 0)
        return true;
    else return false;
}
bool UserDb::CheckExistWithSessionId(std::string sessionId, std::string* name, int* position) {
    char sqlSelect[128];
    sprintf_s(sqlSelect, "SELECT * FROM User WHERE sessionId='%s';", sessionId.c_str());
    //cout << sqlSelect;
    char** results = NULL;
    int rows, columns;
    char* error;
    sqlite3_get_table(db, sqlSelect, &results, &rows, &columns, &error);

    
    if (rows > 0) {
        *name = results[8];
        *position = atoi(results[12]);
        return true;
    }
        
    else return false;
}
bool UserDb::CheckExistWithSessionId(std::string sessionId, std::string* name, int* position, int* department) {
    char sqlSelect[128];
    sprintf_s(sqlSelect, "SELECT * FROM User WHERE sessionId='%s';", sessionId.c_str());
    //cout << sqlSelect;
    char** results = NULL;
    int rows, columns;
    char* error;
    sqlite3_get_table(db, sqlSelect, &results, &rows, &columns, &error);


    if (rows > 0) {
        *name = results[8];
        *position = atoi(results[12]);
        *department = atoi(results[9]);
        return true;
    }

    else return false;
}
bool UserDb::CheckExistWithSessionId(std::string sessionId, int* id) {
    char sqlSelect[128];
    sprintf_s(sqlSelect, "SELECT * FROM User WHERE sessionId='%s';", sessionId.c_str());
    //cout << sqlSelect;
    char** results = NULL;
    int rows, columns;
    char* error;
    sqlite3_get_table(db, sqlSelect, &results, &rows, &columns, &error);

    *id = atoi(results[7]);
    if (rows > 0)
        return true;
    else return false;
}
bool UserDb::CheckExistWithSessionId(std::string sessionId, int* id, int *idDepartment) {
    char sqlSelect[128];
    sprintf_s(sqlSelect, "SELECT * FROM User WHERE sessionId='%s';", sessionId.c_str());
    //cout << sqlSelect;
    char** results = NULL;
    int rows, columns;
    char* error;
    sqlite3_get_table(db, sqlSelect, &results, &rows, &columns, &error);

    *id = atoi(results[7]);
    *idDepartment = atoi(results[9]);
    if (rows > 0)
        return true;
    else return false;
}
bool UserDb::CheckExistWithNameAndPassword(std::string name, std::string password) {
    char sqlSelect[128];
    sprintf_s(sqlSelect, "SELECT * FROM User WHERE email='%s' AND password='%s';", name.c_str(), password.c_str());
    //cout << sqlSelect;
    char** results = NULL;
    int rows, columns;
    char* error;
    sqlite3_get_table(db, sqlSelect, &results, &rows, &columns, &error);

    if (rows > 0)
        return true;
    else return false;
}
bool UserDb::ReadFromEmail(std::string email, User** user) {
    return true;
}
void UserDb::Update(User user) {
    char* error;
    // Execute SQL
    //cout << "DbUpdate ..." << endl;
    char sqlUpdate[256];
    sprintf_s(sqlUpdate, "UPDATE User SET name='%s', password='%s', position=%d, department=%d, email='%s' WHERE id=%d;", user.Name.c_str(), user.Password.c_str(), user.Position, user.Department, user.Email.c_str(), user.Id);
    //cout << sqlUpdate;
    int rc = sqlite3_exec(db, sqlUpdate, NULL, NULL, &error);
    if (rc)
    {
        //cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << endl << endl;
        sqlite3_free(error);
    }
    else
    {
        //cout << "Created firmwareTb." << endl << endl;
    }
}
void UserDb::UpdateSessionId(std::string email, std::string sessionId) {
    char* error;
    // Execute SQL
    //cout << "DbUpdate ..." << endl;
    char sqlUpdate[256];
    sprintf_s(sqlUpdate, "UPDATE User SET sessionId='%s' WHERE email=\"%s\";", sessionId.c_str(), email.c_str());
    //cout << sqlUpdate;
    int rc = sqlite3_exec(db, sqlUpdate, NULL, NULL, &error);
    if (rc)
    {
        //cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << endl << endl;
        sqlite3_free(error);
    }
    else
    {
        //cout << "Created firmwareTb." << endl << endl;
    }
}
void UserDb::Delete(User user) {
    char* error;
    // Execute SQL
    //cout << "DbDelete ..." << endl;
    char sqlDelete[256];
    sprintf_s(sqlDelete, "DELETE FROM User WHERE id=%d;", user.Id);
    //cout << sqlDelete;
    int rc = sqlite3_exec(db, sqlDelete, NULL, NULL, &error);
    if (rc)
    {
        //cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << endl << endl;
        sqlite3_free(error);
    }
    else
    {
        //cout << "Created firmwareTb." << endl << endl;
    }
}