#include "WebServer.h"
#include "config.h"
#include "firmware.h"
#include "./Model/HexFile.h"
#include "UserDb.h"

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "guid2str.h"


// View
#include "View/login/login.h"

using namespace std;
using namespace rapidjson;

HexFileDb hexFileDb;
LoginController loginController;
HomeController homeController;

UserDb userDb;

std::string userIdStr;

void wait() {
	std::this_thread::sleep_for(std::chrono::duration<int, std::ratio<1, 1000>>(200));
}

void WebServer::Init() {
	userDb.Init();
	loginController.Init(&userDb);
	homeController.Init(&userDb);
}
void WebServer::DatabaseInit(void){
	
}

std::string WebServer::GetCookies(std::vector<std::string> dataReceived) {
	std::string cookiesStr = "";

	for (int i = 0; i < dataReceived.size(); i++){
		if (dataReceived[i].substr(0, 7) == "Cookie:") {
			cookiesStr = dataReceived[i].substr(7, dataReceived[i].size() - 7);
		}
	}
	return cookiesStr;
}
// Handler for when a message is received from the client
void WebServer::onMessageReceived(int clientSocket, const char* msg, int length)
{
	// Parse out the client's request string e.g. GET /index.html HTTP/1.1
	std::istringstream iss(msg);
	std::vector<std::string> parsed((std::istream_iterator<std::string>(iss)), std::istream_iterator<std::string>());

	std::vector<std::string> dataReceived = split_string(msg, "\r\n");
	
	std::string cookiesStr = GetCookies(dataReceived);
	// Some defaults for output to the client (404 file not found 'page')
	std::string content = "<h1>404 Not Found</h1>";
	std::string htmlFile = "/index.html";
	int errorCode = 404;
	
	//std::cout << "\r\n\r\n\r\n\r\n\r\n\r\nReceived:\r\n";
	//std::cout << msg;

	#ifdef DISPLAY_DATA_RECEIVED
		for (int i = 0; i < parsed.size(); i++) {
			cout << parsed[i];
		}
	#endif

	std::ostringstream oss;
	// If the GET request is valid, try and get the name
	//if (parsed.size() >= 3 && parsed[0] == "GET")
	#ifdef DEBUG
		cout << "\r\n------------------------------------------------";
		cout << "\r\nNew request, parsed size: "<<parsed.size()<<"\r\n";
	#endif

		if (parsed.size() >= 3)
	{
		htmlFile = parsed[1];

		// If the file is just a slash, use index.html. This should really
		// be if it _ends_ in a slash. I'll leave that for you :)

		/*if (htmlFile == "/"){
			std::ifstream t("introPage.htm");
			std::string str;

			str = loginPageHex;
			errorCode = 200;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Content-Type: text/html\r\n";
			oss << "Content-Length: " << str.size() << "\r\n";
			oss << "\r\n";
			oss << str;

			std::string output = oss.str();
			int size = output.size() + 1;

			sendToClient(clientSocket, output.c_str(), size);
		}
		else */
		if (htmlFile.substr(0, 6) == "/login") {
			std::string contentReponse_t = loginController.Check(htmlFile, cookiesStr, dataReceived[dataReceived.size() - 1]);
			int size = contentReponse_t.size() + 1;
			sendToClient(clientSocket, contentReponse_t.c_str(), size);
		}
		//else if (htmlFile.substr(0, 5) == "/home") {
		else{
			if (htmlFile != "/logo.jpg") {
				std::string contentReponse_t = homeController.Check(htmlFile, cookiesStr, dataReceived[dataReceived.size() - 1]);
				int size = contentReponse_t.size() + 1;
				sendToClient(clientSocket, contentReponse_t.c_str(), size);
			}
			else {
				int size;
				char* contentReponse_t = homeController.Check(htmlFile, cookiesStr, dataReceived[dataReceived.size() - 1], &size);
				sendToClient(clientSocket, contentReponse_t, size);
			}
		}
		/*else if (htmlFile.substr(0, 6) == "/fonts") {
			int size;
			char* contentReponse_t = homeController.GetFont(htmlFile, cookiesStr, dataReceived[dataReceived.size() - 1], &size);
			sendToClient(clientSocket, contentReponse_t, size);
		}
		else {
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Content-Type: text/html\r\n";
			oss << "Content-Length: " << content.size() << "\r\n";
			oss << "\r\n";
			oss << content;

			std::string output = oss.str();
			int size = output.size() + 1;

			sendToClient(clientSocket, output.c_str(), size);
		}*/
	}
	else {

	}	
	

	
}

// Handler for client connections
void WebServer::onClientConnected(int clientSocket)
{

}

// Handler for client disconnections
void WebServer::onClientDisconnected(int clientSocket)
{
	
}