#pragma once
#include <vector>
#include <ctime>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <istream>
#include <sstream>
#include <fstream>
#include <time.h>
#include "Uri.h"
#include "UserDb.h"
#include "View/login/login.h"
#include "TcpListener.h"
#include "guid2str.h"
#include "./Lib/SplitStr/SplitStr.h"
// Json parse library
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

class LoginController
{
public:
	void Init(UserDb* userDb);
	std::string Check(std::string command, std::string cookies, std::string data);
private:
	UserDb *userDb;
};

