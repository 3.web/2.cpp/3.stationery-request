#pragma once
#include <stdio.h>
#include <string>
#include <iostream>
#include "../sqlite3.h"

typedef struct {
	int Id;
	uint32_t Date;
	int IdUser;
	std::string MenuList;
	int Status;
}DinnerMenuRegister;

class DinnerMenuRegisterDb
{
public:
	void Init();
	void Create(DinnerMenuRegister dinnerMenuRegister);
	std::string GetAll();
	std::string Read(int id);
	void Update(DinnerMenuRegister dinnerMenuRegister);
	void UpdateStatus(DinnerMenuRegister dinnerMenuRegister);
	void Delete(DinnerMenuRegister dinnerMenuRegister);
private:
	sqlite3* db;
};

