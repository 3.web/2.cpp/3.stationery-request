#pragma once
#include <stdio.h>
#include <string>
#include <iostream>
#include "../sqlite3.h"

typedef struct {
	int Id;
	char* Name;
}Unit;

class UnitDb
{
public:
	void Init();
	void Create(Unit unit);
	std::string GetAll();
	std::string Read(int id);
	void Update(Unit unit);
	void Delete(Unit unit);
private:
	sqlite3* db;
};

