#pragma once
#include <stdio.h>
#include <string>
#include <iostream>
#include "../sqlite3.h"

typedef struct {
	int Id;
	char* Name;
}LunchMenu;

class LunchMenuDb
{
public:
	void Init();
	void Create(LunchMenu lunchMenu);
	std::string GetAll();
	std::string Read(int id);
	void Update(LunchMenu lunchMenu);
	void Delete(LunchMenu lunchMenu);
private:
	sqlite3* db;
};

