#pragma once
#pragma once
#include <stdio.h>
#include <string>
#include <iostream>
#include "../sqlite3.h"

typedef struct {
	int Id;
	int Date;
	int IdDepartment;
	int IdUserRegister;
	int IdUserCreate;
	int IdDinnerMenu;
}DinnerSheetDetail;

class DinnerSheetDetailDb
{
public:
	void Init();
	void Create(DinnerSheetDetail dinnerSheetDetail);
	std::string GetAll();
	std::string Read(DinnerSheetDetail dinnerSheetDetail);
	void Update(DinnerSheetDetail dinnerSheetDetail);
	void Delete(DinnerSheetDetail dinnerSheetDetail);
	void Delete(int date, int idDepartment);
	void Delete(int date);
private:
	sqlite3* db;
};

