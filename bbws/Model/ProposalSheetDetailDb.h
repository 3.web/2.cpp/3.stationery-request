#pragma once
#include <stdio.h>
#include <string>
#include <iostream>
#include "../sqlite3.h"

typedef struct {
	int Id;
	int IdProposalSheet;
	int IdEquipment;
	int IdUnit;
	int Quantity;
	int IdUserCreate;	
	std::string Link;
}ProposalSheetDetail;

class ProposalSheetDetailDb
{
public:
	void Init();
	void Create(ProposalSheetDetail proposalSheetDetail);
	std::string GetAll();
	std::string Read(ProposalSheetDetail proposalSheetDetail);
	void Update(ProposalSheetDetail proposalSheetDetail);
	void Delete(ProposalSheetDetail proposalSheetDetail);
	void DeleteWithSheetId(int idProposalSheet);
private:
	sqlite3* db;
};

