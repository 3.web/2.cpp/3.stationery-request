#pragma once
#include <stdio.h>
#include <string>
#include <iostream>
#include "../sqlite3.h"

typedef struct {
	int Id;
	char* Name;
}DinnerMenu;

class DinnerMenuDb
{
public:
	void Init();
	void Create(DinnerMenu dinnerMenu);
	std::string GetAll();
	std::string Read(int id);
	void Update(DinnerMenu dinnerMenu);
	void Delete(DinnerMenu dinnerMenu);
private:
	sqlite3* db;
};

