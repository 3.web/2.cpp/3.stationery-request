#pragma once
#pragma once
#include <stdio.h>
#include <string>
#include <iostream>
#include "../sqlite3.h"

typedef struct {
	int Id;
	int IdDepartment;
	std::string Date;
	int IdUser;
	int Status;
}DinnerSheet;

class DinnerSheetDb
{
public:
	void Init();
	void Create(DinnerSheet dinnerSheet);
	std::string GetAll();
	std::string Read(int id);
	void Update(DinnerSheet dinnerSheet);
	void UpdateStatus(DinnerSheet dinnerSheet);
	void Delete(DinnerSheet dinnerSheet);
private:
	sqlite3* db;
};

