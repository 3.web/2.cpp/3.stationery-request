#include "HexFile.h"

HexFileDb::HexFileDb(){

}
HexFileDb::~HexFileDb(){

}
void HexFileDb::Init(void){
  int rc;
  char* error;

  // Open Database
  //std::cout << "Opening upware.db ..." << std::endl;
  rc = sqlite3_open("HexFile.db", &db);
  if (rc)
  {
      std::cerr << "Error opening SQLite3 database: " << sqlite3_errmsg(db) << std::endl << std::endl;
      sqlite3_close(db);
      return;
  }
  else
  {
      //std::cout << "Opened HexFile.db" << std::endl << std::endl;
  }

  // Execute SQL
  //std::cout << "Creating firmwareTb ..." << std::endl;
  const char* sqlCreateTable = "CREATE TABLE HexBlockFile (id INTEGER PRIMARY KEY, firmwareId STRING, fileIndex INTEGER, hexStr STRING, numberOfDownloads INTEGER);";
  //const char* sqlCreateTable = "DROP TABLE firmwareTb;";
  rc = sqlite3_exec(db, sqlCreateTable, NULL, NULL, &error);
  if (rc)
  {
      //std::cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << std::endl << std::endl;
      sqlite3_free(error);
      //return;
  }
  else
  {
      //std::cout << "Created firmwareTb." << std::endl << std::endl;
  }

  /*
      // Execute SQL
      std::cout << "Inserting a value into firmwareTb ..." << std::endl;
      //const char* sqlInsert = "INSERT INTO firmwareTb(userId, firmwareId, name, platform, version, fileCount, downloadCount) VALUES('a6hcH017Q4diBZCmn1KKQcAvMgB2', '55b1f340-f328-427e-bf8a-038e16f55c8f', 'IOT Device 1', 'STM32F103ZE', 'V2.1.0', 25, 1257);";
      const char* sqlInsert = "DELETE FROM firmwareTb WHERE id = 2;";
      rc = sqlite3_exec(db, sqlInsert, NULL, NULL, &error);
      if (rc)
      {
          std::cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << std::endl << std::endl;
          sqlite3_free(error);
      }
      else
      {
          std::cout << "Inserted a value into firmwareTb." << std::endl << std::endl;
      }
  */
  
  /*
      // Execute SQL
      std::cout << "Inserting a value into firmwareTb ..." << std::endl;
      const char* sqlInsert = "INSERT INTO firmwareTb(userId, firmwareId, name, platform, version, fileCount, downloadCount) VALUES('a6hcH017Q4diBZCmn1KKQcAvMgB2', '55b1f340-f328-427e-bf8a-038e16f55c8f', 'IOT Device 1', 'STM32F103ZE', 'V2.1.0', 25, 1257);";
      rc = sqlite3_exec(db, sqlInsert, NULL, NULL, &error);
      if (rc)
      {
          std::cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << std::endl << std::endl;
          sqlite3_free(error);
      }
      else
      {
          std::cout << "Inserted a value into MyTable." << std::endl << std::endl;
      }
  */
  
  #ifdef DEBUG
    std::cout<<"\r\nOpen HexFile Database Successful" << std::endl;;
  #endif
  
  // Display MyTable
  //std::cout << "Retrieving values in firmwareTb ..." << std::endl;
  const char* sqlSelect = "SELECT * FROM HexBlockFile;";
  char** results = NULL;
  int rows, columns;
  sqlite3_get_table(db, sqlSelect, &results, &rows, &columns, &error);
  // Display Table
  for (int rowCtr = 0; rowCtr <= rows; ++rowCtr)
  {
      for (int colCtr = 0; colCtr < columns; ++colCtr)
      {
          // Determine Cell Position
          int cellPosition = (rowCtr * columns) + colCtr;

          if (colCtr == 1) {
              // Display Cell Value
              std::cout.width(36);
              std::cout.setf(std::ios::left);
              std::cout << results[cellPosition] << " ";
          }
          else if (colCtr == 2) {
              // Display Cell Value
              std::cout.width(12);
              std::cout.setf(std::ios::left);
              std::cout << results[cellPosition] << " ";
          }
          else if (colCtr == 3) {
              // Not display hexString
          }
          else {
              // Display Cell Value
              std::cout.width(12);
              std::cout.setf(std::ios::left);
              std::cout << results[cellPosition] << " ";
          }
      }

      // End Line
      std::cout << std::endl;

      // Display Separator For Header
      if (0 == rowCtr)
      {
          for (int colCtr = 0; colCtr < columns; ++colCtr)
          {
              if (colCtr == 1) {
                  std::cout.width(36);
                  std::cout.setf(std::ios::left);
                  std::cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ";
              }
              else if (colCtr == 2) {
                  std::cout.width(12);
                  std::cout.setf(std::ios::left);
                  std::cout << "~~~~~~~~~~~~ ";
              }
              else if (colCtr == 3) {
                  // Not display hexString
              }
              else {
                  std::cout.width(12);
                  std::cout.setf(std::ios::left);
                  std::cout << "~~~~~~~~~~~~ ";
              }
              
          }
          std::cout << std::endl;
      }
  }
  sqlite3_free_table(results);
  sqlite3_free(error);
}
void HexFileDb::Create(HexFileObject hexFileObj){
  char* error;  
  // Execute SQL
  //cout << "Inserting a value into firmwareTb ..." << std::endl;
  //const char* sqlInsert = "INSERT INTO firmwareTb(userId, firmwareId, name, platform, version, fileCount, downloadCount) VALUES('a6hcH017Q4diBZCmn1KKQcAvMgB2', '55b1f340-f328-427e-bf8a-038e16f55c8f', 'IOT Device 1', 'STM32F103ZE', 'V2.1.0', 25, 1257);";
  char sqlInsert[1000000];
  sprintf_s(sqlInsert, "INSERT INTO HexBlockFile(firmwareId, fileIndex, hexStr, numberOfDownloads) VALUES('%s',%d,'%s',%d);", 
      hexFileObj.firmwareId.c_str(), 
      hexFileObj.fileIndex, 
      hexFileObj.hexStr.c_str(), 
      hexFileObj.numberOfDownloads);
  int rc = sqlite3_exec(db, sqlInsert, NULL, NULL, &error);
  if (rc)
  {
      std::cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << std::endl << std::endl;
      sqlite3_free(error);
  }
  else
  {
      //cout << "Inserted a value into firmwareTb." << std::endl << std::endl;
  }
}
std::string HexFileDb::ReadData(HexFileObject hexFileObj){
    // Display MyTable
    //cout << "Retrieving values in firmwareTb ..." << endl;
    char sqlSelect[256];
    sprintf_s(sqlSelect, "SELECT * FROM HexBlockFile WHERE firmwareId='%s' AND fileIndex=%d;", hexFileObj.firmwareId.c_str(), hexFileObj.fileIndex);
    //cout << sqlSelect;
    char** results = NULL;
    int rows, columns;
    char* error;
    sqlite3_get_table(db, sqlSelect, &results, &rows, &columns, &error);
    std::string dataReturn = "";
    // Display Table
    if (rows == 1) {
        dataReturn = results[8];
    }
    else {
        dataReturn = "Error";
    }
    sqlite3_free_table(results);
    sqlite3_free(error);

    //dataGetOfUser = (char*)dataReturn.c_str();
    return dataReturn;
}
void HexFileDb::Update(HexFileObject hexFileObj){
  char* error;
  // Execute SQL
  //cout << "DbUpdate ..." << std::endl;
  char sqlUpdate[256];
  sprintf_s(sqlUpdate, "UPDATE HexBlockFile SET fileIndex=%d, hexStr='%s' WHERE firmwareId='%s';", 
            hexFileObj.fileIndex, 
            hexFileObj.hexStr.c_str(), 
            hexFileObj.firmwareId.c_str());
  //cout << sqlUpdate;
  int rc = sqlite3_exec(db, sqlUpdate, NULL, NULL, &error);
  if (rc)
  {
      std::cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << std::endl << std::endl;
      sqlite3_free(error);
  }
  else
  {
      //cout << "Created firmwareTb." << std::endl << std::endl;
  }
}
void HexFileDb::Delete(HexFileObject hexFileObj){
  char* error;
  // Execute SQL
  //cout << "DbDelete ..." << std::endl;
  char sqlDelete[256];
  sprintf_s(sqlDelete, "DELETE FROM HexBlockFile WHERE firmwareId='%s';", hexFileObj.firmwareId.c_str());
  //cout << sqlDelete;
  int rc = sqlite3_exec(db, sqlDelete, NULL, NULL, &error);
  if (rc)
  {
      std::cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << std::endl << std::endl;
      sqlite3_free(error);
  }
  else
  {
      //cout << "Created firmwareTb." << std::endl << std::endl;
  }
}