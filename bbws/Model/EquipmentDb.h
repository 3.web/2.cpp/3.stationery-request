#pragma once
#include <stdio.h>
#include <string>
#include <iostream>
#include "../sqlite3.h"

typedef struct {
  int Id;
	char* Name;
}Equipment;

class EquipmentDb
{
public:
	void Init();
	void Create(Equipment equipment);
	std::string GetAll();
	std::string Read(int id);
	void Update(Equipment equipment);
	void Delete(Equipment equipment);
private:
	sqlite3* db;
};

