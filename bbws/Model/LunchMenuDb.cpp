#include "LunchMenuDb.h"
#define DEBUG

void LunchMenuDb::Init() {
    int rc;
    char* error;

    // Open Database
    //std::cout << "Opening upware.db ..." << std::endl;
    rc = sqlite3_open("LunchMenuDb.db", &db);
    if (rc)
    {
        std::cerr << "Error opening LunchMenuDb database: " << sqlite3_errmsg(db) << std::endl << std::endl;
        sqlite3_close(db);
        return;
    }
    else
    {
        //std::cout << "Opened HexFile.db" << std::endl << std::endl;
    }

    // Execute SQL
    //std::cout << "Creating firmwareTb ..." << std::endl;
    const char* sqlCreateTable = "CREATE TABLE LunchMenuDb (id INTEGER PRIMARY KEY, name STRING);";
    //const char* sqlCreateTable = "DROP TABLE User;";      // Delete table
    rc = sqlite3_exec(db, sqlCreateTable, NULL, NULL, &error);
    if (rc)
    {
        //std::cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << std::endl << std::endl;
        sqlite3_free(error);
        //return;
    }
    else
    {
        //std::cout << "Created firmwareTb." << std::endl << std::endl;
    }


    // Execute SQL
    //std::cout << "Inserting a value into LunchMenuDb ..." << std::endl;
    //const char* sqlInsert = "INSERT INTO LunchMenuDb(name) VALUES('B�t bi xanh');";
    ////const char* sqlInsert = "DELETE FROM firmwareTb WHERE id = 2;";
    //rc = sqlite3_exec(db, sqlInsert, NULL, NULL, &error);
    //if (rc)
    //{
    //    std::cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << std::endl << std::endl;
    //    sqlite3_free(error);
    //}
    //else
    //{
    //    std::cout << "Inserted a value into firmwareTb." << std::endl << std::endl;
    //}


    /*
        // Execute SQL
        std::cout << "Inserting a value into firmwareTb ..." << std::endl;
        const char* sqlInsert = "INSERT INTO firmwareTb(userId, firmwareId, name, platform, version, fileCount, downloadCount) VALUES('a6hcH017Q4diBZCmn1KKQcAvMgB2', '55b1f340-f328-427e-bf8a-038e16f55c8f', 'IOT Device 1', 'STM32F103ZE', 'V2.1.0', 25, 1257);";
        rc = sqlite3_exec(db, sqlInsert, NULL, NULL, &error);
        if (rc)
        {
            std::cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << std::endl << std::endl;
            sqlite3_free(error);
        }
        else
        {
            std::cout << "Inserted a value into MyTable." << std::endl << std::endl;
        }
    */

#ifdef DEBUG
    std::cout << "\r\nOpen LunchMenuDb Database Successful" << std::endl;;
#endif

    // Display MyTable
    //std::cout << "Retrieving values in firmwareTb ..." << std::endl;
    const char* sqlSelect = "SELECT * FROM LunchMenuDb;";
    char** results = NULL;
    int rows, columns;
    sqlite3_get_table(db, sqlSelect, &results, &rows, &columns, &error);
    // Display Table
    for (int rowCtr = 0; rowCtr <= rows; ++rowCtr)
    {
        for (int colCtr = 0; colCtr < columns; ++colCtr)
        {
            // Determine Cell Position
            int cellPosition = (rowCtr * columns) + colCtr;

            if ((colCtr == 1) || (colCtr == 3)) {
                // Display Cell Value
                std::cout.width(36);
                std::cout.setf(std::ios::left);
                std::cout << results[cellPosition] << " ";
            }
            else if (colCtr == 2) {
                // Display Cell Value
                std::cout.width(12);
                std::cout.setf(std::ios::left);
                std::cout << results[cellPosition] << " ";
            }
            else {
                // Display Cell Value
                std::cout.width(12);
                std::cout.setf(std::ios::left);
                std::cout << results[cellPosition] << " ";
            }
        }

        // End Line
        std::cout << std::endl;

        // Display Separator For Header
        if (0 == rowCtr)
        {
            for (int colCtr = 0; colCtr < columns; ++colCtr)
            {
                if ((colCtr == 1) || (colCtr == 3)) {
                    std::cout.width(36);
                    std::cout.setf(std::ios::left);
                    std::cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ";
                }
                else if (colCtr == 2) {
                    std::cout.width(12);
                    std::cout.setf(std::ios::left);
                    std::cout << "~~~~~~~~~~~~ ";
                }
                else {
                    std::cout.width(12);
                    std::cout.setf(std::ios::left);
                    std::cout << "~~~~~~~~~~~~ ";
                }

            }
            std::cout << std::endl;
        }
    }
    sqlite3_free_table(results);
    sqlite3_free(error);
}
void LunchMenuDb::Create(LunchMenu lunchMenu) {
    char* error;
    // Execute SQL
    //cout << "Inserting a value into firmwareTb ..." << endl;
    //const char* sqlInsert = "INSERT INTO firmwareTb(userId, firmwareId, name, platform, version, fileCount, downloadCount) VALUES('a6hcH017Q4diBZCmn1KKQcAvMgB2', '55b1f340-f328-427e-bf8a-038e16f55c8f', 'IOT Device 1', 'STM32F103ZE', 'V2.1.0', 25, 1257);";
    char sqlInsert[256];
    sprintf_s(sqlInsert, "INSERT INTO LunchMenuDb(name) VALUES('%s');",
        lunchMenu.Name);
    int rc = sqlite3_exec(db, sqlInsert, NULL, NULL, &error);
    if (rc)
    {
        //cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << endl << endl;
        sqlite3_free(error);
    }
    else
    {
        //cout << "Inserted a value into firmwareTb." << endl << endl;
    }
}
std::string LunchMenuDb::GetAll() {
    char sqlSelect[128];
    sprintf_s(sqlSelect, "SELECT * FROM LunchMenuDb;");
    //cout << sqlSelect;
    char** results = NULL;
    int rows, columns;
    char* error;
    sqlite3_get_table(db, sqlSelect, &results, &rows, &columns, &error);

    std::string jsonReturn = "[";
    for (int rowCtr = 1; rowCtr <= rows; ++rowCtr)
    {
        if (rowCtr != 1) {
            jsonReturn += ",";
        }

        int cellPosition = (rowCtr * columns) + 0;
        jsonReturn = jsonReturn + "{\"id\":" + results[cellPosition] + ",\"name\":\"" + results[cellPosition + 1] + "\"}";

        // End Line
        std::cout << std::endl;
    }
    jsonReturn += "]";

    return jsonReturn;
}
std::string LunchMenuDb::Read(int id) {
    char sqlSelect[128];
    sprintf_s(sqlSelect, "SELECT name FROM LunchMenuDb WHERE id=%d;", id);
    //cout << sqlSelect;
    char** results = NULL;
    int rows, columns;
    char* error;
    sqlite3_get_table(db, sqlSelect, &results, &rows, &columns, &error);

    return results[0];
}
void LunchMenuDb::Update(LunchMenu lunchMenu) {
    char* error;
    // Execute SQL
    //cout << "DbUpdate ..." << endl;
    char sqlUpdate[256];
    sprintf_s(sqlUpdate, "UPDATE LunchMenuDb SET name='%s' WHERE id=%d;", lunchMenu.Name, lunchMenu.Id);
    //cout << sqlUpdate;
    int rc = sqlite3_exec(db, sqlUpdate, NULL, NULL, &error);
    if (rc)
    {
        //cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << endl << endl;
        sqlite3_free(error);
    }
    else
    {
        //cout << "Created firmwareTb." << endl << endl;
    }
}
void LunchMenuDb::Delete(LunchMenu lunchMenu) {
    char* error;
    // Execute SQL
    //cout << "DbDelete ..." << endl;
    char sqlDelete[256];
    sprintf_s(sqlDelete, "DELETE FROM LunchMenuDb WHERE id=%d;", lunchMenu.Id);
    //cout << sqlDelete;
    int rc = sqlite3_exec(db, sqlDelete, NULL, NULL, &error);
    if (rc)
    {
        //cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << endl << endl;
        sqlite3_free(error);
    }
    else
    {
        //cout << "Created firmwareTb." << endl << endl;
    }
}