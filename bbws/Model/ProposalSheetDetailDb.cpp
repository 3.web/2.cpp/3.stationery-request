#include "ProposalSheetDetailDb.h"
#define DEBUG
void ProposalSheetDetailDb::Init(){
  int rc;
  char* error;

  // Open Database
  //std::cout << "Opening upware.db ..." << std::endl;
  rc = sqlite3_open("proposalSheetDetailDb.db", &db);
  if (rc)
  {
      std::cerr << "Error opening proposalSheetDetailDb database: " << sqlite3_errmsg(db) << std::endl << std::endl;
      sqlite3_close(db);
      return;
  }
  else
  {
      //std::cout << "Opened HexFile.db" << std::endl << std::endl;
  }

  // Execute SQL
  //std::cout << "Creating firmwareTb ..." << std::endl;
  const char* sqlCreateTable = "CREATE TABLE proposalSheetDetailDb (id INTEGER PRIMARY KEY, idProposalSheet INTEGER, idEquipment INTEGER, idUnit INTEGER, quantity INTEGER, idUserCreate INTEGER, link STRING);";
  //const char* sqlCreateTable = "DROP TABLE proposalSheetDetailDb;";      // Delete table
  rc = sqlite3_exec(db, sqlCreateTable, NULL, NULL, &error);
  if (rc)
  {
      //std::cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << std::endl << std::endl;
      sqlite3_free(error);
      //return;
  }
  else
  {
      //std::cout << "Created firmwareTb." << std::endl << std::endl;
  }

  
  // Execute SQL
  //std::cout << "Inserting a value into proposalSheetDetailDb ..." << std::endl;
  //const char* sqlInsert = "INSERT INTO proposalSheetDetailDb(name) VALUES('Hộp');";
  ////const char* sqlInsert = "DELETE FROM firmwareTb WHERE id = 2;";
  //rc = sqlite3_exec(db, sqlInsert, NULL, NULL, &error);
  //if (rc)
  //{
  //    std::cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << std::endl << std::endl;
  //    sqlite3_free(error);
  //}
  //else
  //{
  //    std::cout << "Inserted a value into firmwareTb." << std::endl << std::endl;
  //}
  

  /*
      // Execute SQL
      std::cout << "Inserting a value into firmwareTb ..." << std::endl;
      const char* sqlInsert = "INSERT INTO firmwareTb(userId, firmwareId, name, platform, version, fileCount, downloadCount) VALUES('a6hcH017Q4diBZCmn1KKQcAvMgB2', '55b1f340-f328-427e-bf8a-038e16f55c8f', 'IOT Device 1', 'STM32F103ZE', 'V2.1.0', 25, 1257);";
      rc = sqlite3_exec(db, sqlInsert, NULL, NULL, &error);
      if (rc)
      {
          std::cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << std::endl << std::endl;
          sqlite3_free(error);
      }
      else
      {
          std::cout << "Inserted a value into MyTable." << std::endl << std::endl;
      }
  */

#ifdef DEBUG
  std::cout << "\r\nOpen proposalSheetDetailDb Database Successful" << std::endl;;
#endif

  // Display MyTable
  //std::cout << "Retrieving values in firmwareTb ..." << std::endl;
  const char* sqlSelect = "SELECT * FROM proposalSheetDetailDb;";
  char** results = NULL;
  int rows, columns;
  sqlite3_get_table(db, sqlSelect, &results, &rows, &columns, &error);
  // Display Table
  for (int rowCtr = 0; rowCtr <= rows; ++rowCtr)
  {
      for (int colCtr = 0; colCtr < columns; ++colCtr)
      {
          // Determine Cell Position
          int cellPosition = (rowCtr * columns) + colCtr;

          if ((colCtr == 1) || (colCtr == 3)) {
              // Display Cell Value
              std::cout.width(36);
              std::cout.setf(std::ios::left);
              std::cout << results[cellPosition] << " ";
          }
          else if (colCtr == 2) {
              // Display Cell Value
              std::cout.width(12);
              std::cout.setf(std::ios::left);
              std::cout << results[cellPosition] << " ";
          }
          else {
              // Display Cell Value
              std::cout.width(12);
              std::cout.setf(std::ios::left);
              std::cout << results[cellPosition] << " ";
          }
      }

      // End Line
      std::cout << std::endl;

      // Display Separator For Header
      if (0 == rowCtr)
      {
          for (int colCtr = 0; colCtr < columns; ++colCtr)
          {
              if ((colCtr == 1)|| (colCtr == 3)) {
                  std::cout.width(36);
                  std::cout.setf(std::ios::left);
                  std::cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ";
              }
              else if (colCtr == 2) {
                  std::cout.width(12);
                  std::cout.setf(std::ios::left);
                  std::cout << "~~~~~~~~~~~~ ";
              }
              else {
                  std::cout.width(12);
                  std::cout.setf(std::ios::left);
                  std::cout << "~~~~~~~~~~~~ ";
              }

          }
          std::cout << std::endl;
      }
  }
  sqlite3_free_table(results);
  sqlite3_free(error);
}
void ProposalSheetDetailDb::Create(ProposalSheetDetail proposalSheetDetail){
  char* error;
  // Execute SQL
  //cout << "Inserting a value into firmwareTb ..." << endl;
  //const char* sqlInsert = "INSERT INTO firmwareTb(userId, firmwareId, name, platform, version, fileCount, downloadCount) VALUES('a6hcH017Q4diBZCmn1KKQcAvMgB2', '55b1f340-f328-427e-bf8a-038e16f55c8f', 'IOT Device 1', 'STM32F103ZE', 'V2.1.0', 25, 1257);";
  char sqlInsert[256];
  sprintf_s(sqlInsert, "INSERT INTO proposalSheetDetailDb(idProposalSheet, idEquipment, idUnit, quantity, idUserCreate, link) VALUES(%d, %d, %d, %d, %d, '%s');",
      proposalSheetDetail.IdProposalSheet, proposalSheetDetail.IdEquipment, proposalSheetDetail.IdUnit, proposalSheetDetail.Quantity, proposalSheetDetail.IdUserCreate, proposalSheetDetail.Link.c_str());
  int rc = sqlite3_exec(db, sqlInsert, NULL, NULL, &error);
  if (rc)
  {
      //cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << endl << endl;
      sqlite3_free(error);
  }
  else
  {
      //cout << "Inserted a value into firmwareTb." << endl << endl;
  }
}
std::string ProposalSheetDetailDb::GetAll() {
    char sqlSelect[128];
    sprintf_s(sqlSelect, "SELECT * FROM proposalSheetDetailDb;");
    //cout << sqlSelect;
    char** results = NULL;
    int rows, columns;
    char* error;
    sqlite3_get_table(db, sqlSelect, &results, &rows, &columns, &error);

    /*std::string jsonReturn = "[";
    for (int rowCtr = 1; rowCtr <= rows; ++rowCtr)
    {
        if (rowCtr != 1) {
            jsonReturn += ",";
        }

        int cellPosition = (rowCtr * columns) + 0;
        jsonReturn = jsonReturn + "{\"id\":" + results[cellPosition] + ",\"idDepartment\":" + results[cellPosition + 1] + ",\"monthYear\":\"" + results[cellPosition + 2] + "\",\"idUser\":" + results[cellPosition + 3] + ",\"status\":" + results[cellPosition + 4] + "}";

        // End Line
        std::cout << std::endl;
    }
    jsonReturn += "]";
    */
    return "";
}
std::string ProposalSheetDetailDb::Read(ProposalSheetDetail proposalSheetDetail){
  char sqlSelect[128];
  sprintf_s(sqlSelect, "SELECT * FROM proposalSheetDetailDb WHERE idProposalSheet=%d;", proposalSheetDetail.IdProposalSheet);
  //cout << sqlSelect;
  char** results = NULL;
  int rows, columns;
  char* error;
  sqlite3_get_table(db, sqlSelect, &results, &rows, &columns, &error);

  std::string jsonReturn = "[";
  for (int rowCtr = 1; rowCtr <= rows; ++rowCtr)
  {
      if (rowCtr != 1) {
          jsonReturn += ",";
      }

      int cellPosition = (rowCtr * columns) + 0;
      jsonReturn = jsonReturn + "{\"id\":" + results[cellPosition] + ",\"idProposalSheet\":\"" + results[cellPosition + 1] + "\",\"idEquipment\":" + results[cellPosition + 2] + ",\"idUnit\":" + results[cellPosition + 3] + ",\"quantity\":" + results[cellPosition + 4] + ",\"idUserCreate\":" + results[cellPosition + 5] + ",\"link\":\"" + results[cellPosition + 6] + "\"}";

      // End Line
      std::cout << std::endl;
  }
  jsonReturn += "]";
  
  return jsonReturn;
}
void ProposalSheetDetailDb::Update(ProposalSheetDetail proposalSheetDetail){
  char* error;
  // Execute SQL
  //cout << "DbUpdate ..." << endl;
  char sqlUpdate[256];
  sprintf_s(sqlUpdate, "UPDATE proposalSheetDetailDb SET idEquipment=%d,idUnit=%d,quantity=%d WHERE id=%d;", proposalSheetDetail.IdEquipment, proposalSheetDetail.IdUnit, proposalSheetDetail.Quantity, proposalSheetDetail.Id);
  //cout << sqlUpdate;
  int rc = sqlite3_exec(db, sqlUpdate, NULL, NULL, &error);
  if (rc)
  {
      //cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << endl << endl;
      sqlite3_free(error);
  }
  else
  {
      //cout << "Created firmwareTb." << endl << endl;
  }
}
void ProposalSheetDetailDb::Delete(ProposalSheetDetail proposalSheetDetail){
  char* error;
  // Execute SQL
  //cout << "DbDelete ..." << endl;
  char sqlDelete[256];
  sprintf_s(sqlDelete, "DELETE FROM proposalSheetDetailDb WHERE id=%d;", proposalSheetDetail.Id);
  //cout << sqlDelete;
  int rc = sqlite3_exec(db, sqlDelete, NULL, NULL, &error);
  if (rc)
  {
      //cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << endl << endl;
      sqlite3_free(error);
  }
  else
  {
      //cout << "Created firmwareTb." << endl << endl;
  }
}
void ProposalSheetDetailDb::DeleteWithSheetId(int idProposalSheet) {
    char* error;
    // Execute SQL
    //cout << "DbDelete ..." << endl;
    char sqlDelete[256];
    sprintf_s(sqlDelete, "DELETE FROM proposalSheetDetailDb WHERE idProposalSheet=%d;", idProposalSheet);
    //cout << sqlDelete;
    int rc = sqlite3_exec(db, sqlDelete, NULL, NULL, &error);
    if (rc)
    {
        //cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << endl << endl;
        sqlite3_free(error);
    }
    else
    {
        //cout << "Created firmwareTb." << endl << endl;
    }
}