#pragma once
#include <stdio.h>
#include <string>
#include <iostream>
#include "../sqlite3.h"

typedef struct {
	int Id;
	char* Name;
}Department;

class DepartmentDb
{
public:
	void Init();
	void Create(Department department);
	std::string GetAll();
	std::string Read(int id);
	void Update(Department department);
	void Delete(Department department);
private:
	sqlite3* db;
};

