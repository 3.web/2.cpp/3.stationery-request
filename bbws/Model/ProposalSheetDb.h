#pragma once
#include <stdio.h>
#include <string>
#include <iostream>
#include "../sqlite3.h"

typedef struct {
	int Id;
	int IdDepartment;
	std::string MonthYear;
	int IdUser;
	int Status;
}ProposalSheet;

class ProposalSheetDb
{
public:
	void Init();
	void Create(ProposalSheet proposalSheet);
	std::string GetAll();
	std::string Read(int id);
	void Update(ProposalSheet proposalSheet);
	void UpdateStatus(ProposalSheet proposalSheet);
	void Delete(ProposalSheet proposalSheet);
private:
	sqlite3* db;
};

