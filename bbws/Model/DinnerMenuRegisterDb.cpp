﻿#include "DinnerMenuRegisterDb.h"
#define DEBUG
void DinnerMenuRegisterDb::Init() {
    int rc;
    char* error;

    // Open Database
    //std::cout << "Opening upware.db ..." << std::endl;
    rc = sqlite3_open("DinnerMenuRegisterDb.db", &db);
    if (rc)
    {
        std::cerr << "Error opening DinnerMenuRegisterDb database: " << sqlite3_errmsg(db) << std::endl << std::endl;
        sqlite3_close(db);
        return;
    }
    else
    {
        //std::cout << "Opened HexFile.db" << std::endl << std::endl;
    }

    // Execute SQL
    //std::cout << "Creating firmwareTb ..." << std::endl;
    const char* sqlCreateTable = "CREATE TABLE DinnerMenuRegisterDb (id INTEGER PRIMARY KEY, Date INTEGER, idUser INTEGER, menuList STRING, status INTEGER);";
    //const char* sqlCreateTable = "DROP TABLE DinnerMenuRegisterDb;";      // Delete table
    rc = sqlite3_exec(db, sqlCreateTable, NULL, NULL, &error);
    if (rc)
    {
        //std::cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << std::endl << std::endl;
        sqlite3_free(error);
        //return;
    }
    else
    {
        //std::cout << "Created firmwareTb." << std::endl << std::endl;
    }


    // Execute SQL
    //std::cout << "Inserting a value into DinnerMenuRegisterDb ..." << std::endl;
    //const char* sqlInsert = "INSERT INTO DinnerMenuRegisterDb(name) VALUES('Hộp');";
    ////const char* sqlInsert = "DELETE FROM firmwareTb WHERE id = 2;";
    //rc = sqlite3_exec(db, sqlInsert, NULL, NULL, &error);
    //if (rc)
    //{
    //    std::cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << std::endl << std::endl;
    //    sqlite3_free(error);
    //}
    //else
    //{
    //    std::cout << "Inserted a value into firmwareTb." << std::endl << std::endl;
    //}


    /*
        // Execute SQL
        std::cout << "Inserting a value into firmwareTb ..." << std::endl;
        const char* sqlInsert = "INSERT INTO firmwareTb(userId, firmwareId, name, platform, version, fileCount, downloadCount) VALUES('a6hcH017Q4diBZCmn1KKQcAvMgB2', '55b1f340-f328-427e-bf8a-038e16f55c8f', 'IOT Device 1', 'STM32F103ZE', 'V2.1.0', 25, 1257);";
        rc = sqlite3_exec(db, sqlInsert, NULL, NULL, &error);
        if (rc)
        {
            std::cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << std::endl << std::endl;
            sqlite3_free(error);
        }
        else
        {
            std::cout << "Inserted a value into MyTable." << std::endl << std::endl;
        }
    */

#ifdef DEBUG
    std::cout << "\r\nOpen DinnerMenuRegisterDb Database Successful" << std::endl;;
#endif

    // Display MyTable
    //std::cout << "Retrieving values in firmwareTb ..." << std::endl;
    const char* sqlSelect = "SELECT * FROM DinnerMenuRegisterDb;";
    char** results = NULL;
    int rows, columns;
    sqlite3_get_table(db, sqlSelect, &results, &rows, &columns, &error);
    // Display Table
    for (int rowCtr = 0; rowCtr <= rows; ++rowCtr)
    {
        for (int colCtr = 0; colCtr < columns; ++colCtr)
        {
            // Determine Cell Position
            int cellPosition = (rowCtr * columns) + colCtr;

            if ((colCtr == 1) || (colCtr == 3)) {
                // Display Cell Value
                std::cout.width(36);
                std::cout.setf(std::ios::left);
                std::cout << results[cellPosition] << " ";
            }
            else if (colCtr == 2) {
                // Display Cell Value
                std::cout.width(12);
                std::cout.setf(std::ios::left);
                std::cout << results[cellPosition] << " ";
            }
            else {
                // Display Cell Value
                std::cout.width(12);
                std::cout.setf(std::ios::left);
                std::cout << results[cellPosition] << " ";
            }
        }

        // End Line
        std::cout << std::endl;

        // Display Separator For Header
        if (0 == rowCtr)
        {
            for (int colCtr = 0; colCtr < columns; ++colCtr)
            {
                if ((colCtr == 1) || (colCtr == 3)) {
                    std::cout.width(36);
                    std::cout.setf(std::ios::left);
                    std::cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ";
                }
                else if (colCtr == 2) {
                    std::cout.width(12);
                    std::cout.setf(std::ios::left);
                    std::cout << "~~~~~~~~~~~~ ";
                }
                else {
                    std::cout.width(12);
                    std::cout.setf(std::ios::left);
                    std::cout << "~~~~~~~~~~~~ ";
                }

            }
            std::cout << std::endl;
        }
    }
    sqlite3_free_table(results);
    sqlite3_free(error);
}
void DinnerMenuRegisterDb::Create(DinnerMenuRegister dinnerMenuRegister) {
    char* error;
    // Execute SQL
    //cout << "Inserting a value into firmwareTb ..." << endl;
    //const char* sqlInsert = "INSERT INTO firmwareTb(userId, firmwareId, name, platform, version, fileCount, downloadCount) VALUES('a6hcH017Q4diBZCmn1KKQcAvMgB2', '55b1f340-f328-427e-bf8a-038e16f55c8f', 'IOT Device 1', 'STM32F103ZE', 'V2.1.0', 25, 1257);";
    char sqlInsert[256];
    char* menuList = (char*)dinnerMenuRegister.MenuList.c_str();
    const char* s = "Adsfa";
    std::string menuListString = dinnerMenuRegister.MenuList;
    sprintf_s(sqlInsert, "INSERT INTO DinnerMenuRegisterDb(Date,  idUser, menuList, status) VALUES(%lu, %d, '%s', %d);",
        dinnerMenuRegister.Date, dinnerMenuRegister.IdUser, menuListString.c_str(), dinnerMenuRegister.Status);
    int rc = sqlite3_exec(db, sqlInsert, NULL, NULL, &error);
    if (rc)
    {
        //cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << endl << endl;
        sqlite3_free(error);
    }
    else
    {
        //cout << "Inserted a value into firmwareTb." << endl << endl;
    }
}
std::string DinnerMenuRegisterDb::GetAll() {
    char sqlSelect[128];
    sprintf_s(sqlSelect, "SELECT * FROM DinnerMenuRegisterDb;");
    //cout << sqlSelect;
    char** results = NULL;
    int rows, columns;
    char* error;
    sqlite3_get_table(db, sqlSelect, &results, &rows, &columns, &error);

    std::string jsonReturn = "[";
    for (int rowCtr = 1; rowCtr <= rows; ++rowCtr)
    {
        if (rowCtr != 1) {
            jsonReturn += ",";
        }

        int cellPosition = (rowCtr * columns) + 0;
        jsonReturn = jsonReturn + "{\"id\":" + results[cellPosition] + ",\"date\":" + results[cellPosition + 1] + ",\"idUser\":" + results[cellPosition + 2] + ",\"menuList\":\"" + results[cellPosition + 3] + "\",\"status\":\"" + results[cellPosition + 4] + "\"}";

        // End Line
        std::cout << std::endl;
    }
    jsonReturn += "]";

    return jsonReturn;
}
std::string DinnerMenuRegisterDb::Read(int id) {
    char sqlSelect[128];
    sprintf_s(sqlSelect, "SELECT name FROM DinnerMenuRegisterDb WHERE id=%d;", id);
    //cout << sqlSelect;
    char** results = NULL;
    int rows, columns;
    char* error;
    sqlite3_get_table(db, sqlSelect, &results, &rows, &columns, &error);

    return results[0];
}
void DinnerMenuRegisterDb::Update(DinnerMenuRegister dinnerMenuRegister) {
    char* error;
    // Execute SQL
    //cout << "DbUpdate ..." << endl;
    char sqlUpdate[256];
    sprintf_s(sqlUpdate, "UPDATE DinnerMenuRegisterDb SET Date=%lu,idUser=%d,menuList='%s' WHERE id=%d;", dinnerMenuRegister.Date, dinnerMenuRegister.IdUser, dinnerMenuRegister.MenuList.c_str(), dinnerMenuRegister.Id);
    //cout << sqlUpdate;
    int rc = sqlite3_exec(db, sqlUpdate, NULL, NULL, &error);
    if (rc)
    {
        //cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << endl << endl;
        sqlite3_free(error);
    }
    else
    {
        //cout << "Created firmwareTb." << endl << endl;
    }
}
void DinnerMenuRegisterDb::UpdateStatus(DinnerMenuRegister dinnerMenuRegister) {
    char* error;
    // Execute SQL
    //cout << "DbUpdate ..." << endl;
    char sqlUpdate[256];
    sprintf_s(sqlUpdate, "UPDATE DinnerMenuRegisterDb SET idUser=%d,status=%d WHERE id=%d;", dinnerMenuRegister.IdUser, dinnerMenuRegister.Status, dinnerMenuRegister.Id);
    //cout << sqlUpdate;
    int rc = sqlite3_exec(db, sqlUpdate, NULL, NULL, &error);
    if (rc)
    {
        //cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << endl << endl;
        sqlite3_free(error);
    }
    else
    {
        //cout << "Created firmwareTb." << endl << endl;
    }
}
void DinnerMenuRegisterDb::Delete(DinnerMenuRegister dinnerMenuRegister) {
    char* error;
    // Execute SQL
    //cout << "DbDelete ..." << endl;
    char sqlDelete[256];
    sprintf_s(sqlDelete, "DELETE FROM DinnerMenuRegisterDb WHERE id=%d;", dinnerMenuRegister.Id);
    //cout << sqlDelete;
    int rc = sqlite3_exec(db, sqlDelete, NULL, NULL, &error);
    if (rc)
    {
        //cerr << "Error executing SQLite3 statement: " << sqlite3_errmsg(db) << endl << endl;
        sqlite3_free(error);
    }
    else
    {
        //cout << "Created firmwareTb." << endl << endl;
    }
}