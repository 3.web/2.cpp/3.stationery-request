#include "Uri.h"

std::string Uri::Decode(const std::string& sSrc)
{
    // Note from RFC1630: "Sequences which start with a percent
    // sign but are not followed by two hexadecimal characters
    // (0-9, A-F) are reserved for future extension"

    const unsigned char* pSrc = (const unsigned char*)sSrc.c_str();
    const int SRC_LEN = sSrc.length();
    const unsigned char* const SRC_END = pSrc + SRC_LEN;
    // last decodable '%' 
    const unsigned char* const SRC_LAST_DEC = SRC_END - 2;

    char* const pStart = new char[SRC_LEN];
    char* pEnd = pStart;

    while (pSrc < SRC_LAST_DEC)
    {
        if (*pSrc == '%')
        {
            char dec1, dec2;
            std::stringstream stream;
            stream << *(pSrc + 1);
            stream >> std::hex >> dec1;
            stream << *(pSrc + 2);
            stream >> std::hex >> dec2;
            if (-1 != dec1 && -1 != dec2)
            {
                *pEnd++ = (dec1 << 4) + dec2;
                pSrc += 3;
                continue;
            }
        }

        *pEnd++ = *pSrc++;
    }

    // the last 2- chars
    while (pSrc < SRC_END)
        *pEnd++ = *pSrc++;

    std::string sResult(pStart, pEnd);
    delete[] pStart;
    return sResult;
}