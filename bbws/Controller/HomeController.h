#pragma once
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <istream>
#include <sstream>
#include <fstream>
#include <time.h>
#include "../Lib/SplitStr/SplitStr.h"
#include "../View/home/home.h"
#include "../View/login/login.h"
#include "../Model/UnitDb.h"
#include "../Model/EquipmentDb.h"
#include "../Model/DepartmentDb.h"
#include "../Model/ProposalSheetDb.h"
#include "../Model/ProposalSheetDetailDb.h"
#include "../Model/DinnerSheetDb.h"
#include "../Model/DinnerSheetDetailDb.h"
#include "../Model/DinnerMenuDb.h"
#include "../Model/DinnerMenuRegisterDb.h"
#include "../Model/LunchMenuDb.h"
#include "../UserDb.h"

#include "../rapidjson/document.h"
#include "../rapidjson/writer.h"
#include "../rapidjson/stringbuffer.h"

class HomeController {
public:
	void Init(UserDb *userDb);
	std::string Check(std::string command, std::string cookies, std::string data);
	char* Check(std::string command, std::string cookies, std::string data, int* size);
	char* GetFont(std::string command, std::string cookies, std::string data, int *size);
private:
	UserDb* userDb;
	UnitDb unitDb;
	EquipmentDb equipmentDb;
	DepartmentDb departmentDb;
	ProposalSheetDb proposalSheetDb;
	ProposalSheetDetailDb proposalSheetDetailDb;
	DinnerSheetDb dinnerSheetDb;
	DinnerSheetDetailDb dinnerSheetDetailDb;
	DinnerMenuDb dinnerMenuDb;
	DinnerMenuRegisterDb dinnerMenuRegisterDb;
	LunchMenuDb lunchMenuDb;
};