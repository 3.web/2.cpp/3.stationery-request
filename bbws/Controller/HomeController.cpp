#include "HomeController.h"
Home home;
char dataReturn[512000];
#pragma warning(disable : 4996) //_CRT_SECURE_NO_WARNINGS

void HomeController::Init(UserDb* userDb) {
	this->userDb = userDb;
	unitDb.Init();
	equipmentDb.Init();
	departmentDb.Init();
	proposalSheetDb.Init();
	proposalSheetDetailDb.Init();
	dinnerSheetDb.Init();
	dinnerSheetDetailDb.Init();
	dinnerMenuDb.Init();
	dinnerMenuRegisterDb.Init();
	lunchMenuDb.Init();
}
extern char fontawesome_webfont_woff2_Hex[];
extern char fontawesome_webfont_woff_Hex[];
extern char fontawesome_webfont_ttf_Hex[];
extern char logo_jpg_Hex[87551];
std::string HomeController::Check(std::string command, std::string cookies, std::string data) {
	// Some defaults for output to the client (404 file not found 'page')
	std::string content = "<h1>404 Not Found</h1>";
	std::string htmlFile = "/index.html";
	int errorCode = 404;

	//std::cout << "\r\n\r\n\r\n\r\n\r\n\r\nReceived:\r\n";
	//std::cout << msg;



	std::ostringstream oss;
	std::string contentStr;

	if (command == "/") {
		// Filter cookies and return "sessionId" cookies
		cookies.erase(std::remove_if(cookies.begin(), cookies.end(), isspace), cookies.end());
		std::vector<std::string> cookiesArr = split_string(cookies, ";");

		std::string cookiesSessionId = "";
		for (int element = 0; element < cookiesArr.size(); element++) {
			if (cookiesArr[element].substr(0, 10) == "sessionId=") {
				cookiesSessionId = cookiesArr[element].substr(10, cookiesArr[element].size() - 10);
				break;
			}
		}
		std::string str;

		// Check length of sessionId string: "sessionId=..." and Check sessionId on database
		if ((cookiesSessionId.size() == 36)&&(this->userDb->CheckExistWithSessionId(cookiesSessionId))) {
			errorCode = 200;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Content-Type: text/html\r\n";
			contentStr = home.index_html();

			std::ifstream t("../View/home/index.html");
			std::string str;

			t.seekg(0, std::ios::end);
			str.reserve(t.tellg());
			t.seekg(0, std::ios::beg);

			str.assign((std::istreambuf_iterator<char>(t)),
				std::istreambuf_iterator<char>());

			errorCode = 200;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Content-Type: text/html\r\n";
			oss << "Content-Length: " << str.size() << "\r\n";
			oss << "\r\n";
			oss << str;
		}
		else {
			errorCode = 301;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Location: /login \r\n\r\n";			
			std::string output = oss.str();
			int size = output.size() + 1;

			return output;
		}
	}
	else if (command == "/getData") {
		// Filter cookies and return "sessionId" cookies
		cookies.erase(std::remove_if(cookies.begin(), cookies.end(), isspace), cookies.end());
		std::vector<std::string> cookiesArr = split_string(cookies, ";");

		std::string cookiesSessionId = "";
		for (int element = 0; element < cookiesArr.size(); element++) {
			if (cookiesArr[element].substr(0, 10) == "sessionId=") {
				cookiesSessionId = cookiesArr[element].substr(10, cookiesArr[element].size() - 10);
				break;
			}
		}

		std::string nameOfSessionId;
		int position, department;
		if ((cookiesSessionId.size() == 36) && (this->userDb->CheckExistWithSessionId(cookiesSessionId, &nameOfSessionId, &position, &department))) {
			errorCode = 200;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Content-Type: text/html\r\n";
			//oss << "Set-Cookie: userName="<< nameOfSessionId<<"\r\n";
			std::string positionStr = std::to_string(position);
			std::string departmentStr = std::to_string(department);
			contentStr = "[" + this->proposalSheetDb.GetAll() + "," + this->equipmentDb.GetAll() + "," + this->unitDb.GetAll() + "," + this->departmentDb.GetAll() + "," + this->userDb->GetAll() + ",[{\"name\":\"" + nameOfSessionId + "\"},{\"position\":" + positionStr + "},{\"department\":" + departmentStr + "}]," + this->dinnerSheetDb.GetAll() + "," + this->dinnerMenuDb.GetAll() + "," + this->dinnerMenuRegisterDb.GetAll() + "," + this->dinnerSheetDetailDb.GetAll() + "," + this->lunchMenuDb.GetAll() + "]";
		}
		else {

		}
		
	}
	else if (command == "/crProposalSheetDetail") {
		// Filter cookies and return "sessionId" cookies
		cookies.erase(std::remove_if(cookies.begin(), cookies.end(), isspace), cookies.end());
		std::vector<std::string> cookiesArr = split_string(cookies, ";");

		std::string cookiesSessionId = "";
		for (int element = 0; element < cookiesArr.size(); element++) {
			if (cookiesArr[element].substr(0, 10) == "sessionId=") {
				cookiesSessionId = cookiesArr[element].substr(10, cookiesArr[element].size() - 10);
				break;
			}
		}

		int idOfSessionId;
		if ((cookiesSessionId.size() == 36) && (this->userDb->CheckExistWithSessionId(cookiesSessionId, &idOfSessionId))) {
			rapidjson::Document d;
			d.Parse(data.c_str());

			ProposalSheetDetail proposalSheetDetail;
			proposalSheetDetail.IdProposalSheet = d["idProposalSheet"].GetInt();
			proposalSheetDetail.IdUserCreate = idOfSessionId;
			proposalSheetDetail.IdEquipment = d["idEquipment"].GetInt();
			proposalSheetDetail.IdUnit = d["idUnit"].GetInt();
			proposalSheetDetail.Quantity = d["quantity"].GetInt();
			proposalSheetDetailDb.Create(proposalSheetDetail);

			errorCode = 200;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Content-Type: text/html\r\n";
			contentStr = this->proposalSheetDetailDb.Read(proposalSheetDetail);
		}
		else {

		}
	}
	else if (command == "/updateProposalSheetDetail") {
		// Filter cookies and return "sessionId" cookies
		cookies.erase(std::remove_if(cookies.begin(), cookies.end(), isspace), cookies.end());
		std::vector<std::string> cookiesArr = split_string(cookies, ";");

		std::string cookiesSessionId = "";
		for (int element = 0; element < cookiesArr.size(); element++) {
			if (cookiesArr[element].substr(0, 10) == "sessionId=") {
				cookiesSessionId = cookiesArr[element].substr(10, cookiesArr[element].size() - 10);
				break;
			}
		}

		int idOfSessionId;
		if ((cookiesSessionId.size() == 36) && (this->userDb->CheckExistWithSessionId(cookiesSessionId, &idOfSessionId))) {
			rapidjson::Document d;
			d.Parse(data.c_str());

			ProposalSheetDetail proposalSheetDetail;
			proposalSheetDetail.Id = d["id"].GetInt();
			proposalSheetDetail.IdProposalSheet = d["idProposalSheet"].GetInt();
			proposalSheetDetail.IdUserCreate = idOfSessionId;
			proposalSheetDetail.IdEquipment = d["idEquipment"].GetInt();
			proposalSheetDetail.IdUnit = d["idUnit"].GetInt();
			proposalSheetDetail.Quantity = d["quantity"].GetInt();
			proposalSheetDetailDb.Update(proposalSheetDetail);

			errorCode = 200;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Content-Type: text/html\r\n";
			contentStr = this->proposalSheetDetailDb.Read(proposalSheetDetail);
		}
		else {

		}
	}
	else if (command == "/rmProposalSheetDetail") {
		// Filter cookies and return "sessionId" cookies
		cookies.erase(std::remove_if(cookies.begin(), cookies.end(), isspace), cookies.end());
		std::vector<std::string> cookiesArr = split_string(cookies, ";");

		std::string cookiesSessionId = "";
		for (int element = 0; element < cookiesArr.size(); element++) {
			if (cookiesArr[element].substr(0, 10) == "sessionId=") {
				cookiesSessionId = cookiesArr[element].substr(10, cookiesArr[element].size() - 10);
				break;
			}
		}

		int idOfSessionId;
		if ((cookiesSessionId.size() == 36) && (this->userDb->CheckExistWithSessionId(cookiesSessionId, &idOfSessionId))) {
			rapidjson::Document d;
			d.Parse(data.c_str());

			ProposalSheetDetail proposalSheetDetail;
			proposalSheetDetail.Id = d["id"].GetInt();
			proposalSheetDetail.IdProposalSheet = d["idProposalSheet"].GetInt();
			proposalSheetDetailDb.Delete(proposalSheetDetail);

			errorCode = 200;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Content-Type: text/html\r\n";
			contentStr = this->proposalSheetDetailDb.Read(proposalSheetDetail);
		}
		else {

		}
	}
	else if (command == "/getProposalSheetDetail") {
		// Filter cookies and return "sessionId" cookies
		cookies.erase(std::remove_if(cookies.begin(), cookies.end(), isspace), cookies.end());
		std::vector<std::string> cookiesArr = split_string(cookies, ";");

		std::string cookiesSessionId = "";
		for (int element = 0; element < cookiesArr.size(); element++) {
			if (cookiesArr[element].substr(0, 10) == "sessionId=") {
				cookiesSessionId = cookiesArr[element].substr(10, cookiesArr[element].size() - 10);
				break;
			}
		}

		std::string nameOfSessionId;
		if ((cookiesSessionId.size() == 36) && (this->userDb->CheckExistWithSessionId(cookiesSessionId, &nameOfSessionId))) {
			rapidjson::Document d;
			d.Parse(data.c_str());

			ProposalSheetDetail proposalSheetDetail;
			proposalSheetDetail.IdProposalSheet = d["idProposalSheet"].GetInt();

			errorCode = 200;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Content-Type: text/html\r\n";
			//oss << "Set-Cookie: userName=" << nameOfSessionId << "\r\n";
			contentStr = this->proposalSheetDetailDb.Read(proposalSheetDetail);
		}
		else {

		}
	}
	else if (command == "/crProposalSheet") {
		cookies.erase(std::remove_if(cookies.begin(), cookies.end(), isspace), cookies.end());
		std::vector<std::string> cookiesArr = split_string(cookies, ";");

		std::string cookiesSessionId = "";
		for (int element = 0; element < cookiesArr.size(); element++) {
			if (cookiesArr[element].substr(0, 10) == "sessionId=") {
				cookiesSessionId = cookiesArr[element].substr(10, cookiesArr[element].size() - 10);
				break;
			}
		}

		int idUserOfSessionId;
		if ((cookiesSessionId.size() == 36) && (this->userDb->CheckExistWithSessionId(cookiesSessionId, &idUserOfSessionId))) {
			time_t now = time(0);

			rapidjson::Document d;
			d.Parse(data.c_str());

			ProposalSheet proposalSheet;
			proposalSheet.IdDepartment = d["name"].GetInt();
			proposalSheet.IdUser = idUserOfSessionId;
			proposalSheet.MonthYear = std::to_string(now);
			proposalSheet.Status = 0;
			this->proposalSheetDb.Create(proposalSheet);

			errorCode = 200;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Content-Type: text/html\r\n";
			contentStr = this->proposalSheetDb.GetAll();
		}
		else {

		}
	}
	else if (command == "/updateProposalSheet") {
		cookies.erase(std::remove_if(cookies.begin(), cookies.end(), isspace), cookies.end());
		std::vector<std::string> cookiesArr = split_string(cookies, ";");

		std::string cookiesSessionId = "";
		for (int element = 0; element < cookiesArr.size(); element++) {
			if (cookiesArr[element].substr(0, 10) == "sessionId=") {
				cookiesSessionId = cookiesArr[element].substr(10, cookiesArr[element].size() - 10);
				break;
			}
		}

		int idUserOfSessionId;
		if ((cookiesSessionId.size() == 36) && (this->userDb->CheckExistWithSessionId(cookiesSessionId, &idUserOfSessionId))) {
			time_t now = time(0);

			rapidjson::Document d;
			d.Parse(data.c_str());

			ProposalSheet proposalSheet;
			proposalSheet.Id = d["id"].GetInt();
			proposalSheet.Status = d["status"].GetInt();
			this->proposalSheetDb.UpdateStatus(proposalSheet);

			errorCode = 200;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Content-Type: text/html\r\n";
			contentStr = this->proposalSheetDb.GetAll();
		}
		else {

		}
	}
	else if (command == "/rmProposalSheet") {
		cookies.erase(std::remove_if(cookies.begin(), cookies.end(), isspace), cookies.end());
		std::vector<std::string> cookiesArr = split_string(cookies, ";");

		std::string cookiesSessionId = "";
		for (int element = 0; element < cookiesArr.size(); element++) {
			if (cookiesArr[element].substr(0, 10) == "sessionId=") {
				cookiesSessionId = cookiesArr[element].substr(10, cookiesArr[element].size() - 10);
				break;
			}
		}

		int idUserOfSessionId;
		if ((cookiesSessionId.size() == 36) && (this->userDb->CheckExistWithSessionId(cookiesSessionId, &idUserOfSessionId))) {
			time_t now = time(0);

			rapidjson::Document d;
			d.Parse(data.c_str());

			ProposalSheet proposalSheet;
			proposalSheet.Id = d["id"].GetInt();
			this->proposalSheetDb.Delete(proposalSheet);

			// Remove item on detail with id of proposal sheet
			this->proposalSheetDetailDb.DeleteWithSheetId(proposalSheet.Id);

			errorCode = 200;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Content-Type: text/html\r\n";
			contentStr = this->proposalSheetDb.GetAll();
		}
		else {

		}
	}
	else if (command == "/crDinnerSheet") {
		cookies.erase(std::remove_if(cookies.begin(), cookies.end(), isspace), cookies.end());
		std::vector<std::string> cookiesArr = split_string(cookies, ";");

		std::string cookiesSessionId = "";
		for (int element = 0; element < cookiesArr.size(); element++) {
			if (cookiesArr[element].substr(0, 10) == "sessionId=") {
				cookiesSessionId = cookiesArr[element].substr(10, cookiesArr[element].size() - 10);
				break;
			}
		}

		int idUserOfSessionId;
		if ((cookiesSessionId.size() == 36) && (this->userDb->CheckExistWithSessionId(cookiesSessionId, &idUserOfSessionId))) {
			time_t now = time(0);

			rapidjson::Document d;
			d.Parse(data.c_str());

			DinnerSheet dinnerSheet;
			dinnerSheet.IdDepartment = d["name"].GetInt();
			dinnerSheet.IdUser = idUserOfSessionId;
			dinnerSheet.Date = std::to_string(now);
			dinnerSheet.Status = 0;
			this->dinnerSheetDb.Create(dinnerSheet);

			errorCode = 200;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Content-Type: text/html\r\n";
			contentStr = this->dinnerSheetDb.GetAll();
		}
		else {
			 
		}
	}
	else if (command == "/updateDinnerSheet") {
		cookies.erase(std::remove_if(cookies.begin(), cookies.end(), isspace), cookies.end());
		std::vector<std::string> cookiesArr = split_string(cookies, ";");

		std::string cookiesSessionId = "";
		for (int element = 0; element < cookiesArr.size(); element++) {
			if (cookiesArr[element].substr(0, 10) == "sessionId=") {
				cookiesSessionId = cookiesArr[element].substr(10, cookiesArr[element].size() - 10);
				break;
			}
		}

		int idUserOfSessionId;
		if ((cookiesSessionId.size() == 36) && (this->userDb->CheckExistWithSessionId(cookiesSessionId, &idUserOfSessionId))) {
			time_t now = time(0);

			rapidjson::Document d;
			d.Parse(data.c_str());

			DinnerSheet dinnerSheet;
			dinnerSheet.Id = d["id"].GetInt();
			dinnerSheet.Status = d["status"].GetInt();
			this->dinnerSheetDb.UpdateStatus(dinnerSheet);

			errorCode = 200;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Content-Type: text/html\r\n";
			contentStr = this->dinnerSheetDb.GetAll();
		}
		else {

		}
	}
	else if (command == "/crDinnerSheetDetail") {
		cookies.erase(std::remove_if(cookies.begin(), cookies.end(), isspace), cookies.end());
		std::vector<std::string> cookiesArr = split_string(cookies, ";");

		std::string cookiesSessionId = "";
		for (int element = 0; element < cookiesArr.size(); element++) {
			if (cookiesArr[element].substr(0, 10) == "sessionId=") {
				cookiesSessionId = cookiesArr[element].substr(10, cookiesArr[element].size() - 10);
				break;
			}
		}

		int idUserOfSessionId;
		int idDepartment;
		if ((cookiesSessionId.size() == 36) && (this->userDb->CheckExistWithSessionId(cookiesSessionId, &idUserOfSessionId, &idDepartment))) {
			time_t now = time(0);

			rapidjson::Document d;
			d.Parse(data.c_str());

			DinnerSheetDetail dinnerSheetDetail;
			dinnerSheetDetail.Date = d["d"].GetInt();
			dinnerSheetDetail.IdDepartment = idDepartment;
			dinnerSheetDetail.IdUserRegister = d["u"].GetInt();
			dinnerSheetDetail.IdUserCreate = idUserOfSessionId;
			dinnerSheetDetail.IdDinnerMenu = d["m"].GetInt();
			this->dinnerSheetDetailDb.Create(dinnerSheetDetail);

			errorCode = 200;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Content-Type: text/html\r\n";
			contentStr = this->dinnerSheetDetailDb.GetAll();
		}
		else {

		}
	}
	else if (command == "/rmDinnerDetailRegister") {
		cookies.erase(std::remove_if(cookies.begin(), cookies.end(), isspace), cookies.end());
		std::vector<std::string> cookiesArr = split_string(cookies, ";");

		std::string cookiesSessionId = "";
		for (int element = 0; element < cookiesArr.size(); element++) {
			if (cookiesArr[element].substr(0, 10) == "sessionId=") {
				cookiesSessionId = cookiesArr[element].substr(10, cookiesArr[element].size() - 10);
				break;
			}
		}

		int idUserOfSessionId;
		if ((cookiesSessionId.size() == 36) && (this->userDb->CheckExistWithSessionId(cookiesSessionId, &idUserOfSessionId))) {
			time_t now = time(0);

			rapidjson::Document d;
			d.Parse(data.c_str());

			DinnerSheetDetail dinnerSheetDetail;
			dinnerSheetDetail.Id = d["id"].GetInt();
			this->dinnerSheetDetailDb.Delete(dinnerSheetDetail);

			errorCode = 200;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Content-Type: text/html\r\n";
			contentStr = this->dinnerSheetDetailDb.GetAll();
		}
		else {

		}
	}
	else if (command == "/crDinnerMenuRegister") {
		cookies.erase(std::remove_if(cookies.begin(), cookies.end(), isspace), cookies.end());
		std::vector<std::string> cookiesArr = split_string(cookies, ";");

		std::string cookiesSessionId = "";
		for (int element = 0; element < cookiesArr.size(); element++) {
			if (cookiesArr[element].substr(0, 10) == "sessionId=") {
				cookiesSessionId = cookiesArr[element].substr(10, cookiesArr[element].size() - 10);
				break;
			}
		}

		int idUserOfSessionId;
		if ((cookiesSessionId.size() == 36) && (this->userDb->CheckExistWithSessionId(cookiesSessionId, &idUserOfSessionId))) {
			time_t now = time(0);

			rapidjson::Document d;
			d.Parse(data.c_str());

			DinnerMenuRegister dinnerMenuRegister;
			dinnerMenuRegister.Date = (uint32_t)d["date"].GetUint64();
			dinnerMenuRegister.MenuList = d["menuList"].GetString();
			dinnerMenuRegister.Status = d["status"].GetInt();
			dinnerMenuRegister.IdUser = idUserOfSessionId;
			this->dinnerMenuRegisterDb.Create(dinnerMenuRegister);

			errorCode = 200;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Content-Type: text/html\r\n";
			contentStr = this->dinnerMenuRegisterDb.GetAll();
		}
		else {

		}
	}
	else if (command == "/updateDinnerMenuRegister") {
		cookies.erase(std::remove_if(cookies.begin(), cookies.end(), isspace), cookies.end());
		std::vector<std::string> cookiesArr = split_string(cookies, ";");

		std::string cookiesSessionId = "";
		for (int element = 0; element < cookiesArr.size(); element++) {
			if (cookiesArr[element].substr(0, 10) == "sessionId=") {
				cookiesSessionId = cookiesArr[element].substr(10, cookiesArr[element].size() - 10);
				break;
			}
		}

		int idUserOfSessionId;
		if ((cookiesSessionId.size() == 36) && (this->userDb->CheckExistWithSessionId(cookiesSessionId, &idUserOfSessionId))) {
			time_t now = time(0);

			rapidjson::Document d;
			d.Parse(data.c_str());

			DinnerMenuRegister dinnerMenuRegister;
			dinnerMenuRegister.Date = (uint32_t)d["date"].GetUint64();
			dinnerMenuRegister.MenuList = d["menuList"].GetString();
			dinnerMenuRegister.IdUser = idUserOfSessionId;
			dinnerMenuRegister.Id = (int)d["id"].GetInt();
			this->dinnerMenuRegisterDb.Update(dinnerMenuRegister);

			errorCode = 200;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Content-Type: text/html\r\n";
			contentStr = this->dinnerMenuRegisterDb.GetAll();
		}
		else {

		}
	}
	else if (command == "/updateStatusMenuRegister") {
		cookies.erase(std::remove_if(cookies.begin(), cookies.end(), isspace), cookies.end());
		std::vector<std::string> cookiesArr = split_string(cookies, ";");

		std::string cookiesSessionId = "";
		for (int element = 0; element < cookiesArr.size(); element++) {
			if (cookiesArr[element].substr(0, 10) == "sessionId=") {
				cookiesSessionId = cookiesArr[element].substr(10, cookiesArr[element].size() - 10);
				break;
			}
		}

		int idUserOfSessionId;
		if ((cookiesSessionId.size() == 36) && (this->userDb->CheckExistWithSessionId(cookiesSessionId, &idUserOfSessionId))) {
			time_t now = time(0);

			rapidjson::Document d;
			d.Parse(data.c_str());

			DinnerMenuRegister dinnerMenuRegister;
			dinnerMenuRegister.IdUser = idUserOfSessionId;
			dinnerMenuRegister.Id = (int)d["id"].GetInt();
			dinnerMenuRegister.Status = (int)d["status"].GetInt();
			this->dinnerMenuRegisterDb.UpdateStatus(dinnerMenuRegister);

			errorCode = 200;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Content-Type: text/html\r\n";
			contentStr = this->dinnerMenuRegisterDb.GetAll();
		}
		else {

		}
	}
	else if (command == "/rmDinnerMenuRegister") {
		cookies.erase(std::remove_if(cookies.begin(), cookies.end(), isspace), cookies.end());
		std::vector<std::string> cookiesArr = split_string(cookies, ";");

		std::string cookiesSessionId = "";
		for (int element = 0; element < cookiesArr.size(); element++) {
			if (cookiesArr[element].substr(0, 10) == "sessionId=") {
				cookiesSessionId = cookiesArr[element].substr(10, cookiesArr[element].size() - 10);
				break;
			}
		}

		int idUserOfSessionId;
		if ((cookiesSessionId.size() == 36) && (this->userDb->CheckExistWithSessionId(cookiesSessionId, &idUserOfSessionId))) {
			time_t now = time(0);

			rapidjson::Document d;
			d.Parse(data.c_str());

			DinnerMenuRegister dinnerMenuRegister;
			dinnerMenuRegister.Id = (int)d["id"].GetInt();
			this->dinnerMenuRegisterDb.Delete(dinnerMenuRegister);

			errorCode = 200;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Content-Type: text/html\r\n";
			contentStr = this->dinnerMenuRegisterDb.GetAll();
		}
		else {

		}
	}
	else if (command == "/crEquipment") {
		Equipment equipment;

		rapidjson::Document d;
		d.Parse(data.c_str());

		std::string nameStr = d["name"].GetString();
		equipment.Name = (char*)nameStr.c_str();

		this->equipmentDb.Create(equipment);

		errorCode = 200;
		oss << "HTTP/1.1 " << errorCode << " OK\r\n";
		oss << "Cache-Control: no-cache, private\r\n";
		oss << "Content-Type: text/html\r\n";
		contentStr = this->equipmentDb.GetAll();
	}
	else if (command == "/updateEquipment") {
		Equipment equipment;

		rapidjson::Document d;
		d.Parse(data.c_str());

		equipment.Id = d["id"].GetInt();
		std::string nameStr = d["name"].GetString();
		equipment.Name = (char*)nameStr.c_str();

		this->equipmentDb.Update(equipment);

		errorCode = 200;
		oss << "HTTP/1.1 " << errorCode << " OK\r\n";
		oss << "Cache-Control: no-cache, private\r\n";
		oss << "Content-Type: text/html\r\n";
		contentStr = this->equipmentDb.GetAll();
	} 
	else if (command == "/rmEquipment") {
		Equipment equipment;

		rapidjson::Document d;
		d.Parse(data.c_str());

		equipment.Id = d["id"].GetInt();
		std::string nameStr = d["name"].GetString();
		equipment.Name = (char*)nameStr.c_str();

		this->equipmentDb.Delete(equipment);

		errorCode = 200;
		oss << "HTTP/1.1 " << errorCode << " OK\r\n";
		oss << "Cache-Control: no-cache, private\r\n";
		oss << "Content-Type: text/html\r\n";
		contentStr = this->equipmentDb.GetAll();
	}
	else if (command == "/crUnit") {
		Unit unit;

		rapidjson::Document d;
		d.Parse(data.c_str());

		std::string nameStr = d["name"].GetString();
		unit.Name = (char*)nameStr.c_str();

		this->unitDb.Create(unit);

		errorCode = 200;
		oss << "HTTP/1.1 " << errorCode << " OK\r\n";
		oss << "Cache-Control: no-cache, private\r\n";
		oss << "Content-Type: text/html\r\n";
		contentStr = this->unitDb.GetAll();
	}
	else if (command == "/updateUnit") {
		Unit unit;

		rapidjson::Document d;
		d.Parse(data.c_str());

		unit.Id = d["id"].GetInt();
		std::string nameStr = d["name"].GetString();
		unit.Name = (char*)nameStr.c_str();

		this->unitDb.Update(unit);

		errorCode = 200;
		oss << "HTTP/1.1 " << errorCode << " OK\r\n";
		oss << "Cache-Control: no-cache, private\r\n";
		oss << "Content-Type: text/html\r\n";
		contentStr = this->unitDb.GetAll();
	}
	else if (command == "/rmUnit") {
		Unit unit;

		rapidjson::Document d;
		d.Parse(data.c_str());

		unit.Id = d["id"].GetInt();
		std::string nameStr = d["name"].GetString();
		unit.Name = (char*)nameStr.c_str();

		this->unitDb.Delete(unit);

		errorCode = 200;
		oss << "HTTP/1.1 " << errorCode << " OK\r\n";
		oss << "Cache-Control: no-cache, private\r\n";
		oss << "Content-Type: text/html\r\n";
		contentStr = this->unitDb.GetAll();
	}
	else if (command == "/crDepartment") {
		Department department;

		rapidjson::Document d;
		d.Parse(data.c_str());

		std::string nameStr = d["name"].GetString();
		department.Name = (char*)nameStr.c_str();

		this->departmentDb.Create(department);

		errorCode = 200;
		oss << "HTTP/1.1 " << errorCode << " OK\r\n";
		oss << "Cache-Control: no-cache, private\r\n";
		oss << "Content-Type: text/html\r\n";
		contentStr = this->departmentDb.GetAll();
	}
	else if (command == "/updateDepartment") {
		Department department;

		rapidjson::Document d;
		d.Parse(data.c_str());

		department.Id = d["id"].GetInt();
		std::string nameStr = d["name"].GetString();
		department.Name = (char*)nameStr.c_str();

		this->departmentDb.Update(department);

		errorCode = 200;
		oss << "HTTP/1.1 " << errorCode << " OK\r\n";
		oss << "Cache-Control: no-cache, private\r\n";
		oss << "Content-Type: text/html\r\n";
		contentStr = this->departmentDb.GetAll();
	}
	else if (command == "/rmDepartment") {
		Department department;

		rapidjson::Document d;
		d.Parse(data.c_str());

		department.Id = d["id"].GetInt();
		std::string nameStr = d["name"].GetString();
		department.Name = (char*)nameStr.c_str();

		this->departmentDb.Delete(department);

		errorCode = 200;
		oss << "HTTP/1.1 " << errorCode << " OK\r\n";
		oss << "Cache-Control: no-cache, private\r\n";
		oss << "Content-Type: text/html\r\n";
		contentStr = this->departmentDb.GetAll();
	}
	else if (command == "/crUser") {
		// Filter cookies and return "sessionId" cookies
		cookies.erase(std::remove_if(cookies.begin(), cookies.end(), isspace), cookies.end());
		std::vector<std::string> cookiesArr = split_string(cookies, ";");

		std::string cookiesSessionId = "";
		for (int element = 0; element < cookiesArr.size(); element++) {
			if (cookiesArr[element].substr(0, 10) == "sessionId=") {
				cookiesSessionId = cookiesArr[element].substr(10, cookiesArr[element].size() - 10);
				break;
			}
		}

		int idOfSessionId;
		if ((cookiesSessionId.size() == 36) && (this->userDb->CheckExistWithSessionId(cookiesSessionId, &idOfSessionId))) {
			rapidjson::Document d;
			d.Parse(data.c_str());

			User user;
			user.Name = d["name"].GetString();
			user.Email = d["email"].GetString();
			user.Password = d["password"].GetString();
			user.Department = d["department"].GetInt();
			user.Position = static_cast<UserPosition>(d["position"].GetInt());
			this->userDb->Create(user);

			errorCode = 200;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Content-Type: text/html\r\n";
			contentStr = this->userDb->GetAll();
		}
		else {

		}
	}
	else if (command == "/updateUser") {
		// Filter cookies and return "sessionId" cookies
		cookies.erase(std::remove_if(cookies.begin(), cookies.end(), isspace), cookies.end());
		std::vector<std::string> cookiesArr = split_string(cookies, ";");

		std::string cookiesSessionId = "";
		for (int element = 0; element < cookiesArr.size(); element++) {
			if (cookiesArr[element].substr(0, 10) == "sessionId=") {
				cookiesSessionId = cookiesArr[element].substr(10, cookiesArr[element].size() - 10);
				break;
			}
		}

		int idOfSessionId;
		if ((cookiesSessionId.size() == 36) && (this->userDb->CheckExistWithSessionId(cookiesSessionId, &idOfSessionId))) {
			rapidjson::Document d;
			d.Parse(data.c_str());

			User user;
			user.Id = d["id"].GetInt();
			user.Name = d["name"].GetString();
			user.Email = d["email"].GetString();
			user.Password = d["password"].GetString();
			user.Department = d["department"].GetInt();
			user.Position = static_cast<UserPosition>(d["position"].GetInt());
			this->userDb->Update(user);

			errorCode = 200;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Content-Type: text/html\r\n";
			contentStr = this->userDb->GetAll();
		}
		else {

		}
	}
	else if (command == "/rmUser") {
	// Filter cookies and return "sessionId" cookies
	cookies.erase(std::remove_if(cookies.begin(), cookies.end(), isspace), cookies.end());
	std::vector<std::string> cookiesArr = split_string(cookies, ";");

	std::string cookiesSessionId = "";
	for (int element = 0; element < cookiesArr.size(); element++) {
		if (cookiesArr[element].substr(0, 10) == "sessionId=") {
			cookiesSessionId = cookiesArr[element].substr(10, cookiesArr[element].size() - 10);
			break;
		}
	}

	int idOfSessionId;
	if ((cookiesSessionId.size() == 36) && (this->userDb->CheckExistWithSessionId(cookiesSessionId, &idOfSessionId))) {
		rapidjson::Document d;
		d.Parse(data.c_str());

		User user;
		user.Id = d["id"].GetInt();
		if (user.Id != 1) {
			this->userDb->Delete(user);
		}

		errorCode = 200;
		oss << "HTTP/1.1 " << errorCode << " OK\r\n";
		oss << "Cache-Control: no-cache, private\r\n";
		oss << "Content-Type: text/html\r\n";
		contentStr = this->userDb->GetAll();
	}
	else {

	}
	}
	else if (command == "/crDinnerMenu") {
		// Filter cookies and return "sessionId" cookies
		cookies.erase(std::remove_if(cookies.begin(), cookies.end(), isspace), cookies.end());
		std::vector<std::string> cookiesArr = split_string(cookies, ";");

		std::string cookiesSessionId = "";
		for (int element = 0; element < cookiesArr.size(); element++) {
			if (cookiesArr[element].substr(0, 10) == "sessionId=") {
				cookiesSessionId = cookiesArr[element].substr(10, cookiesArr[element].size() - 10);
				break;
			}
		}

		int idOfSessionId;
		if ((cookiesSessionId.size() == 36) && (this->userDb->CheckExistWithSessionId(cookiesSessionId, &idOfSessionId))) {
			DinnerMenu dinnerMenu;

			rapidjson::Document d;
			d.Parse(data.c_str());

			std::string nameStr = d["name"].GetString();
			dinnerMenu.Name = (char*)nameStr.c_str();

			this->dinnerMenuDb.Create(dinnerMenu);

			errorCode = 200;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Content-Type: text/html\r\n";
			contentStr = this->dinnerMenuDb.GetAll();
		}
		else {

		}
	}
	else if (command == "/updateDinnerMenu") {
		// Filter cookies and return "sessionId" cookies
		cookies.erase(std::remove_if(cookies.begin(), cookies.end(), isspace), cookies.end());
		std::vector<std::string> cookiesArr = split_string(cookies, ";");

		std::string cookiesSessionId = "";
		for (int element = 0; element < cookiesArr.size(); element++) {
			if (cookiesArr[element].substr(0, 10) == "sessionId=") {
				cookiesSessionId = cookiesArr[element].substr(10, cookiesArr[element].size() - 10);
				break;
			}
		}

		int idOfSessionId;
		if ((cookiesSessionId.size() == 36) && (this->userDb->CheckExistWithSessionId(cookiesSessionId, &idOfSessionId))) {
			DinnerMenu dinnerMenu;

			rapidjson::Document d;
			d.Parse(data.c_str());

			dinnerMenu.Id = d["id"].GetInt();
			std::string nameStr = d["name"].GetString();
			dinnerMenu.Name = (char*)nameStr.c_str();

			this->dinnerMenuDb.Update(dinnerMenu);

			errorCode = 200;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Content-Type: text/html\r\n";
			contentStr = this->dinnerMenuDb.GetAll();
		}
		else {

		}
	}
	else if (command == "/rmDinnerMenu") {
		// Filter cookies and return "sessionId" cookies
		cookies.erase(std::remove_if(cookies.begin(), cookies.end(), isspace), cookies.end());
		std::vector<std::string> cookiesArr = split_string(cookies, ";");

		std::string cookiesSessionId = "";
		for (int element = 0; element < cookiesArr.size(); element++) {
			if (cookiesArr[element].substr(0, 10) == "sessionId=") {
				cookiesSessionId = cookiesArr[element].substr(10, cookiesArr[element].size() - 10);
				break;
			}
		}

		int idOfSessionId;
		if ((cookiesSessionId.size() == 36) && (this->userDb->CheckExistWithSessionId(cookiesSessionId, &idOfSessionId))) {
			DinnerMenu dinnerMenu;

			rapidjson::Document d;
			d.Parse(data.c_str());

			dinnerMenu.Id = d["id"].GetInt();
			std::string nameStr = d["name"].GetString();
			dinnerMenu.Name = (char*)nameStr.c_str();

			this->dinnerMenuDb.Delete(dinnerMenu);

			errorCode = 200;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Content-Type: text/html\r\n";
			contentStr = this->dinnerMenuDb.GetAll();
		}
		else {

		}
	}
	else if (command == "/crLunchMenu") {
		// Filter cookies and return "sessionId" cookies
		cookies.erase(std::remove_if(cookies.begin(), cookies.end(), isspace), cookies.end());
		std::vector<std::string> cookiesArr = split_string(cookies, ";");

		std::string cookiesSessionId = "";
		for (int element = 0; element < cookiesArr.size(); element++) {
			if (cookiesArr[element].substr(0, 10) == "sessionId=") {
				cookiesSessionId = cookiesArr[element].substr(10, cookiesArr[element].size() - 10);
				break;
			}
		}

		int idOfSessionId;
		if ((cookiesSessionId.size() == 36) && (this->userDb->CheckExistWithSessionId(cookiesSessionId, &idOfSessionId))) {
			LunchMenu lunchMenu;

			rapidjson::Document d;
			d.Parse(data.c_str());

			std::string nameStr = d["name"].GetString();
			lunchMenu.Name = (char*)nameStr.c_str();

			this->lunchMenuDb.Create(lunchMenu);

			errorCode = 200;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Content-Type: text/html\r\n";
			contentStr = this->lunchMenuDb.GetAll();
		}
		else {

		}
	}
	else if (command == "/updateLunchMenu") {
		// Filter cookies and return "sessionId" cookies
		cookies.erase(std::remove_if(cookies.begin(), cookies.end(), isspace), cookies.end());
		std::vector<std::string> cookiesArr = split_string(cookies, ";");

		std::string cookiesSessionId = "";
		for (int element = 0; element < cookiesArr.size(); element++) {
			if (cookiesArr[element].substr(0, 10) == "sessionId=") {
				cookiesSessionId = cookiesArr[element].substr(10, cookiesArr[element].size() - 10);
				break;
			}
		}

		int idOfSessionId;
		if ((cookiesSessionId.size() == 36) && (this->userDb->CheckExistWithSessionId(cookiesSessionId, &idOfSessionId))) {
			LunchMenu lunchMenu;

			rapidjson::Document d;
			d.Parse(data.c_str());

			lunchMenu.Id = d["id"].GetInt();
			std::string nameStr = d["name"].GetString();
			lunchMenu.Name = (char*)nameStr.c_str();

			this->lunchMenuDb.Update(lunchMenu);

			errorCode = 200;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Content-Type: text/html\r\n";
			contentStr = this->lunchMenuDb.GetAll();
		}
		else {

		}
	}
	else if (command == "/rmLunchMenu") {
		// Filter cookies and return "sessionId" cookies
		cookies.erase(std::remove_if(cookies.begin(), cookies.end(), isspace), cookies.end());
		std::vector<std::string> cookiesArr = split_string(cookies, ";");

		std::string cookiesSessionId = "";
		for (int element = 0; element < cookiesArr.size(); element++) {
			if (cookiesArr[element].substr(0, 10) == "sessionId=") {
				cookiesSessionId = cookiesArr[element].substr(10, cookiesArr[element].size() - 10);
				break;
			}
		}

		int idOfSessionId;
		if ((cookiesSessionId.size() == 36) && (this->userDb->CheckExistWithSessionId(cookiesSessionId, &idOfSessionId))) {
			LunchMenu lunchMenu;

			rapidjson::Document d;
			d.Parse(data.c_str());

			lunchMenu.Id = d["id"].GetInt();
			std::string nameStr = d["name"].GetString();
			lunchMenu.Name = (char*)nameStr.c_str();

			this->lunchMenuDb.Delete(lunchMenu);

			errorCode = 200;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Content-Type: text/html\r\n";
			contentStr = this->lunchMenuDb.GetAll();
		}
		else {

		}
	}
	else if (command == "/logout") {
		errorCode = 200;
		oss << "HTTP/1.1 " << errorCode << " OK\r\n";
		oss << "Cache-Control: no-cache, private\r\n";
		oss << "Content-Type: text/html\r\n";
		oss << "Set-Cookie: sessionId=null;Path=/\r\n";
		contentStr = "{\"status\":\"Done\"}";
	}
	else if (command == "/home/popper.min.js") {
		errorCode = 200;
		oss << "HTTP/1.1 " << errorCode << " OK\r\n";
		oss << "Cache-Control: public, max-age=31536000\r\n";
		oss << "Content-Type: text/javascript\r\n";
		contentStr = home.popper_min_js();
	}
	else if (command == "/home/bootstrap.min.css") {
		errorCode = 200;
		oss << "HTTP/1.1 " << errorCode << " OK\r\n";
		oss << "Cache-Control: public, max-age=31536000\r\n";
		oss << "Content-Type: text/css\r\n";
		contentStr = home.bootstrap_min_css();
	}
	else if (command == "/home/jquery.min.js") {
		errorCode = 200;
		oss << "HTTP/1.1 " << errorCode << " OK\r\n";
		oss << "Cache-Control: public, max-age=31536000\r\n";
		oss << "Content-Type: text/javascript\r\n";
		contentStr = home.jquery_min_js();
	}
	else if (command == "/home/bootstrap.min.js") {
		errorCode = 200;
		oss << "HTTP/1.1 " << errorCode << " OK\r\n";
		oss << "Cache-Control: public, max-age=31536000\r\n";
		oss << "Content-Type: text/javascript\r\n";
		contentStr = home.bootstrap_min_js();
	}
	else if (command == "/home/font-awesome.min.css") {
		errorCode = 200;
		oss << "HTTP/1.1 " << errorCode << " OK\r\n";
		oss << "Cache-Control: public, max-age=31536000\r\n";
		oss << "Content-Type: text/css\r\n";
		contentStr = home.font_awesome_min_css();
	}
	else if (command == "/home/logo.jpg") {
		errorCode = 200;
		oss << "HTTP/1.1 " << errorCode << " OK\r\n";
		oss << "Cache-Control: public, max-age=31536000\r\n";
		oss << "Content-Type: text/jpeg\r\n";
		contentStr = home.logo_jpg();
	}


	oss << "Content-Length: " << contentStr.size() << "\r\n";
	oss << "\r\n";
	oss << contentStr;
	std::string output = oss.str();
	int size = output.size() + 1;

	return output;
}
char* HomeController::Check(std::string command, std::string cookies, std::string data, int* size) {
	// Some defaults for output to the client (404 file not found 'page')
	std::string content = "<h1>404 Not Found</h1>";
	std::string htmlFile = "/index.html";
	int errorCode = 404;

	//std::cout << "\r\n\r\n\r\n\r\n\r\n\r\nReceived:\r\n";
	//std::cout << msg;



	std::ostringstream oss;

	errorCode = 200;
	oss << "HTTP/1.1 " << errorCode << " OK\r\n";
	oss << "Cache-Control: public, max-age=31536000\r\n";


	if (command == "/home/logo.jpg") {
		oss << "Content-Type: text/jpeg\r\n";

		int contentLength = (sizeof(logo_jpg_Hex) / sizeof(logo_jpg_Hex[0]));
		oss << "Content-Length: " << contentLength << "\r\n";
		oss << "\r\n";

		std::string output = oss.str();
		*size = output.size() + 1 + contentLength;

		const char* data = output.c_str();
		int indexAssign;
		for (indexAssign = 0; indexAssign < output.size(); indexAssign++) {
			dataReturn[indexAssign] = data[indexAssign];
		}
		for (int i = 0; i < contentLength; i++) {
			dataReturn[indexAssign + i] = logo_jpg_Hex[i];
		}
	}




	return dataReturn;
}
char* HomeController::GetFont(std::string command, std::string cookies, std::string data, int* size) {
	// Some defaults for output to the client (404 file not found 'page')
	std::string content = "<h1>404 Not Found</h1>";
	std::string htmlFile = "/index.html";
	int errorCode = 404;

	//std::cout << "\r\n\r\n\r\n\r\n\r\n\r\nReceived:\r\n";
	//std::cout << msg;



	std::ostringstream oss;
	std::string contentStr;

	errorCode = 200;
	oss << "HTTP/1.1 " << errorCode << " OK\r\n";
	oss << "Cache-Control: public, max-age=31536000\r\n";


	if (command == "/fonts/fontawesome-webfont.woff2?v=4.7.0") {
		oss << "Content-Type: font/woff\r\n";
		contentStr = home.fontawesome_webfont_woff();

		//int contentLength = (sizeof(fontawesome_webfont_woff2_Hex) / sizeof(fontawesome_webfont_woff2_Hex[0]));
		int contentLength = 1;
		oss << "Content-Length: " << contentLength << "\r\n";
		oss << "\r\n";

		std::string output = oss.str();
		*size = output.size() + 1 + contentLength;

		const char* data = output.c_str();
		int indexAssign;
		for (indexAssign = 0; indexAssign < output.size(); indexAssign++) {
			dataReturn[indexAssign] = data[indexAssign];
		}
		for (int i = 0; i < contentLength; i++){
			dataReturn[indexAssign + i] = fontawesome_webfont_woff2_Hex[i];
		}
	}
	/*else if (command == "/fonts/fontawesome-webfont.woff?v=4.7.0") {
		oss << "Content-Type: font/woff\r\n";
		contentStr = home.fontawesome_webfont_woff();

		int contentLength = (sizeof(fontawesome_webfont_woff_Hex) / sizeof(fontawesome_webfont_woff_Hex[0]));
		oss << "Content-Length: " << contentLength << "\r\n";
		oss << "\r\n";

		std::string output = oss.str();
		*size = output.size() + 1 + contentLength;

		const char* data = output.c_str();
		int indexAssign;
		for (indexAssign = 0; indexAssign < output.size(); indexAssign++) {
			dataReturn[indexAssign] = data[indexAssign];
		}
		for (int i = 0; i < contentLength; i++) {
			dataReturn[indexAssign + i] = fontawesome_webfont_woff_Hex[i];
		}
	}
	else if (command == "/fonts/fontawesome-webfont.ttf?v=4.7.0") {
		oss << "Content-Type: font/woff\r\n";
		contentStr = home.fontawesome_webfont_woff();

		int contentLength = (sizeof(fontawesome_webfont_ttf_Hex) / sizeof(fontawesome_webfont_ttf_Hex[0]));
		oss << "Content-Length: " << contentLength << "\r\n";
		oss << "\r\n";

		std::string output = oss.str();
		*size = output.size() + 1 + contentLength;

		const char* data = output.c_str();
		int indexAssign;
		for (indexAssign = 0; indexAssign < output.size(); indexAssign++) {
			dataReturn[indexAssign] = data[indexAssign];
		}
		for (int i = 0; i < contentLength; i++) {
			dataReturn[indexAssign + i] = fontawesome_webfont_ttf_Hex[i];
		}
	}*/


	

	return dataReturn;
}