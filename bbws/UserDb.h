#pragma once
#include "./Model/HexFile.h"
#include "./sqlite3.h"
#include <stdio.h>

typedef enum {
	ADMIN = 0,
	STAFF,
	HR,
}UserPosition;

typedef struct {
	int Id;
	std::string Name;
	std::string Password;
	std::string Email;
	UserPosition Position;
	int Department;
	std::string SessionId;
}User;

class UserDb
{
public:
	void Init();
	void Create(User user);
	std::string GetAll();
	bool CheckExistWithSessionId(std::string sessionId);
	bool CheckExistWithSessionId(std::string sessionId, std::string* name);
	bool CheckExistWithSessionId(std::string sessionId, std::string* name, int* position);
	bool CheckExistWithSessionId(std::string sessionId, std::string* name, int* position, int* department);
	bool CheckExistWithSessionId(std::string sessionId, int* id);
	bool CheckExistWithSessionId(std::string sessionId, int* id, int* idDepartment);
	bool CheckExistWithNameAndPassword(std::string name, std::string password);
	bool ReadFromEmail(std::string email, User** user);
	void Update(User user);
	void UpdateSessionId(std::string email, std::string sessionId);
	void Delete(User user);
private:
	sqlite3* db;
};

