#pragma once
#include <string>
#include <istream>
#include <sstream>
#include <fstream>
#include <iostream>
#include <streambuf>
#include <vector>
#include <iterator>
#include <thread> 
#include "TcpListener.h"
#include "LoginController.h"

#include "./Controller/HomeController.h"
#include "./View/home/home.h"

#include "./Lib/SplitStr/SplitStr.h"

class WebServer : public TcpListener
{
public:

	WebServer(const char* ipAddress, int port) :
		TcpListener(ipAddress, port) { }
	void Init();
	void DatabaseInit(void);
protected:

	// Handler for client connections
	virtual void onClientConnected(int clientSocket);

	// Handler for client disconnections
	virtual void onClientDisconnected(int clientSocket);

	// Handler for when a message is received from the client
	virtual void onMessageReceived(int clientSocket, const char* msg, int length);
private:
	std::string GetCookies(std::vector<std::string> dataReceived);
};