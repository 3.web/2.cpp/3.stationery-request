#include "LoginController.h"

void LoginController::Init(UserDb* userDb) {
	this->userDb = userDb;
}
std::string LoginController::Check(std::string command, std::string cookies, std::string data) {
	// Some defaults for output to the client (404 file not found 'page')
	std::string content = "<h1>404 Not Found</h1>";
	std::string htmlFile = "/index.html";
	int errorCode = 404;

	//std::cout << "\r\n\r\n\r\n\r\n\r\n\r\nReceived:\r\n";
	//std::cout << msg;



	std::ostringstream oss;
	std::string str;

	
	

	if (command == "/login") {
		

		// Filter cookies and return "sessionId" cookies
		cookies.erase(std::remove_if(cookies.begin(), cookies.end(), isspace), cookies.end());
		std::vector<std::string> cookiesArr = split_string(cookies, ";");

		std::string cookiesSessionId = "";
		for (int element = 0; element < cookiesArr.size(); element++) {
			if (cookiesArr[element].substr(0, 10) == "sessionId=") {
				cookiesSessionId = cookiesArr[element].substr(10, cookiesArr[element].size() - 10);
				break;
			}
		}

		// Check length of sessionId string: "sessionId=..." and Check sessionId on database
		if ((cookiesSessionId.size() == 36)&&(userDb->CheckExistWithSessionId(cookiesSessionId))) {
			errorCode = 301;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Location: / \r\n\r\n";
			std::string output = oss.str();
			int size = output.size() + 1;
			return output;
		}
		else {
			std::ifstream t("../View/login/index.html");
			std::string strPage;

			t.seekg(0, std::ios::end);
			strPage.reserve(t.tellg());
			t.seekg(0, std::ios::beg);

			strPage.assign((std::istreambuf_iterator<char>(t)),
				std::istreambuf_iterator<char>());

			errorCode = 200;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Content-Type: text/html\r\n";
			str = strPage;
		}		
	}
	else if (command == "/login/check") {
		// Parse content data
		rapidjson::Document d;
		d.Parse(data.c_str());

		// Check user and password on database
		if (userDb->CheckExistWithNameAndPassword(d["user"].GetString(), d["password"].GetString())) {
			GUID gidReference;
			HRESULT hCreateGuid = CoCreateGuid(&gidReference);

			std::string temp = GuidToString(gidReference);
			std::string guidStr = temp.substr(1, temp.size() - 2);
			userDb->UpdateSessionId(d["user"].GetString(), guidStr);

			/*errorCode = 302;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Set-Cookie: sessionId=" << guidStr << ";Path=/\r\n";
			oss << "Location: /home \r\n\r\n";
			std::string output = oss.str();
			int size = output.size() + 1;
			return output;*/
			struct tm newtime;
			time_t now = time(0);
			localtime_s(&newtime, &now);
			newtime.tm_year++;

			char dateTime_string[100];
			strftime(dateTime_string, 50, "%B %d, %Y %T", &newtime);

			errorCode = 200;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Set-Cookie: sessionId=" << guidStr << ";Path=/;Expires="<<dateTime_string<<"\r\n";
			oss << "Content-Type: text/html\r\n";
			str = "{\"status\":\"Done\"}";
		}
		else {
			errorCode = 200;
			oss << "HTTP/1.1 " << errorCode << " OK\r\n";
			oss << "Cache-Control: no-cache, private\r\n";
			oss << "Content-Type: text/html\r\n";
			str = "Login Fail";
		}
	}


	oss << "Content-Length: " << str.size() << "\r\n";
	oss << "\r\n";
	oss << str;
	std::string output = oss.str();
	int size = output.size() + 1;

	return output;
}