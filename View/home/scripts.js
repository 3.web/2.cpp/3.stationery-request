function logout(){
    $.ajax({
        type: 'POST',
        //contentType: "application/json; charset=utf-8",
        dataType: 'json',
        url: '/logout',
        data: JSON.stringify({
            'data': 'null'
        }),
        success: function (msg) {
            //alert('wow' + msg);
            window.location.href = "/login";
        }
    });
}
//--------------------------------------------------------------------------------------
// Create proposal sheet register
function crRegister(){
    urlStr = "/crProposalSheet";

    dataSend = {'name': DbData[3][document.getElementById("departmentList").selectedIndex].id};
    
    $.ajax({
        type: 'POST',
        //contentType: "application/json; charset=utf-8",
        dataType: 'json',
        url: urlStr,
        data: JSON.stringify(dataSend),
        success: function (msg) {
            DbData[tabSelect - 1] = msg;
            buildTableEquipmentOrUnit();
            updateDepartmentOnCreateAndEditModal();
            console.log(DbData[tabSelect - 1]);
        },
        error: function(msg){
            console.warn(msg);
        }
    });
}
function updateRegister(id, status){
    urlStr = "/updateProposalSheet";

    dataSend = {'id': id,
                'status': status};
    
    $.ajax({
        type: 'POST',
        //contentType: "application/json; charset=utf-8",
        dataType: 'json',
        url: urlStr,
        data: JSON.stringify(dataSend),
        success: function (msg) {
            DbData[tabSelect - 1] = msg;
            buildTableEquipmentOrUnit();
        },
        error: function(msg){
            console.warn(msg);
        }
    });
}
function rmRegister(id){
    urlStr = "/rmProposalSheet";

    dataSend = {'id': id};
    
    $.ajax({
        type: 'POST',
        //contentType: "application/json; charset=utf-8",
        dataType: 'json',
        url: urlStr,
        data: JSON.stringify(dataSend),
        success: function (msg) {
            DbData[tabSelect - 1] = msg;
            buildTableEquipmentOrUnit();
        },
        error: function(msg){
            console.warn(msg);
        }
    });
}
function crProposalDetail(){
    if(document.getElementById('quantityDetail').value == ''){
        document.getElementById('quantityDetail').classList.add("red-border");
        return;
    }
    $("#modalCreateDetail").modal('hide');

    urlStr = "/crProposalSheetDetail";

    dataSend = {
        'idProposalSheet': proposalSheetIndexOpenned,
        'idEquipment': DbData[1][document.getElementById("equipmentList").selectedIndex].id,
        'idUnit': DbData[2][document.getElementById("unitList").selectedIndex].id,
        'quantity': parseInt(document.getElementById('quantityDetail').value)
    };
    //document.getElementById("departmentList").selectedIndex
    //document.getElementById("departmentList").value
    
    $.ajax({
        type: 'POST',
        //contentType: "application/json; charset=utf-8",
        dataType: 'json',
        url: urlStr,
        data: JSON.stringify(dataSend),
        success: function (msg) {
            buildTableProposalSheetDetail(msg);
        },
        error: function(msg){
            console.warn(msg);
        }
    });
}
function updateProposalDetail(){
    if(document.getElementById('modalEditDetailquantityDetail').value == ''){
        document.getElementById('modalEditDetailquantityDetail').value = document.getElementById('modalEditDetailquantityDetail').placeholder;            
    }
    $("#modalEditDetail").modal('hide');

    urlStr = "/updateProposalSheetDetail";

    dataSend = {
        'id':idSelectEdit,
        'idProposalSheet': proposalSheetIndexOpenned,
        'idEquipment': DbData[1][document.getElementById("modalEditDetailequipmentList").selectedIndex].id,
        'idUnit': DbData[2][document.getElementById("modalEditDetailunitList").selectedIndex].id,
        'quantity': parseInt(document.getElementById('modalEditDetailquantityDetail').value)
    };

    $.ajax({
        type: 'POST',
        //contentType: "application/json; charset=utf-8",
        dataType: 'json',
        url: urlStr,
        data: JSON.stringify(dataSend),
        success: function (msg) {
            buildTableProposalSheetDetail(msg);
        },
        error: function(msg){
            console.warn(msg);
        }
    });
}
function rmProposalDetail(){
    urlStr = "/rmProposalSheetDetail";

    dataSend = {
        'id':idSelectEdit,
        'idProposalSheet': proposalSheetIndexOpenned
    };

    $.ajax({
        type: 'POST',
        //contentType: "application/json; charset=utf-8",
        dataType: 'json',
        url: urlStr,
        data: JSON.stringify(dataSend),
        success: function (msg) {
            buildTableProposalSheetDetail(msg);
        },
        error: function(msg){
            console.warn(msg);
        }
    });
}
//--------------------------------------------------------------------------------------
// User
var idUserEdit = 0;
function crUser(){
    urlStr = "/crUser";
    console.log('adfasdf')
    dataSend = {'name': document.getElementById('modalCreateUserName').value,
                'email': document.getElementById('modalCreateUserEmail').value,
                'password': document.getElementById('modalCreateUserPassword').value,
                'position': document.getElementById('modalCreateUserSelectPosition').selectedIndex
    };
    
    $.ajax({
        type: 'POST',
        //contentType: "application/json; charset=utf-8",
        dataType: 'json',
        url: urlStr,
        data: JSON.stringify(dataSend),
        success: function (msg) {
            DbData[4] = msg;
            buildTableUser();
        },
        error: function(msg){
            console.warn(msg);
        }
    });
}
function updateUser(){
    urlStr = "/updateUser";
    contentObj = document.getElementById('modalCreateUserContentEdit');
    dataSend = {'id': idUserEdit,
                'name': contentObj[1].value==''?contentObj[1].placeholder:contentObj[1].value,
                'email': contentObj[3].value==''?contentObj[3].placeholder:contentObj[3].value,
                'password': contentObj[5].value==''?contentObj[5].placeholder:contentObj[5].value,
                'position': document.getElementById('modalCreateUserSelectPosition').selectedIndex
    };
    
    $.ajax({
        type: 'POST',
        //contentType: "application/json; charset=utf-8",
        dataType: 'json',
        url: urlStr,
        data: JSON.stringify(dataSend),
        success: function (msg) {
            DbData[4] = msg;
            buildTableUser();
        },
        error: function(msg){
            console.warn(msg);
        }
    });
}
function rmUser(){
    urlStr = "/rmUser";
    contentObj = document.getElementById('modalCreateUserContentEdit');
    dataSend = {'id': idUserEdit};
    
    $.ajax({
        type: 'POST',
        //contentType: "application/json; charset=utf-8",
        dataType: 'json',
        url: urlStr,
        data: JSON.stringify(dataSend),
        success: function (msg) {
            DbData[4] = msg;
            buildTableUser();
        },
        error: function(msg){
            console.warn(msg);
        }
    });
}

function buildDataModalCreateUser(){
    document.getElementById('modalCreateUser').children[0].children[0].children[0].innerText = "Thêm nhân viên mới";
    document.getElementById('createOrEditUserFooter').innerHTML = '<button type="button" class="btn btn-warning" data-dismiss="modal" style="margin-right:30px">Close</button><button id="createUser" type="button" class="btn btn-success" data-dismiss="modal">Create</button>';
    document.getElementById('createUser').addEventListener('click', crUser);
    
    document.getElementById('modalCreateUserName').placeholder = '';
    document.getElementById('modalCreateUserEmail').placeholder = '';
    document.getElementById('modalCreateUserPassword').placeholder = '';
}
function buildDataModalUpdateUser(idUser){
    idUserEdit = idUser;
    console.log(idUser)
    
    document.getElementById('createOrEditUserFooter').innerHTML = '<button type="button" class="btn btn-warning" data-dismiss="modal" style="margin-right:30px">Close</button><button id="rmUser" type="button" class="btn btn-danger" data-dismiss="modal" style="margin-right:30px">Delete</button><button id="updateUser" type="button" class="btn btn-success" data-dismiss="modal" >Update</button>';
    document.getElementById('modalCreateUser').children[0].children[0].children[0].innerText = "Sửa thông tin nhân viên";

    // Find data user with id user
    for(let i = 0; i < DbData[4].length; i++){
        if(DbData[4][i].id == idUser){
            document.getElementById('modalCreateUserName').value = '';                
            document.getElementById('modalCreateUserName').placeholder = DbData[4][i].name;
            document.getElementById('modalCreateUserEmail').value = '';
            document.getElementById('modalCreateUserEmail').placeholder = DbData[4][i].email;
            document.getElementById('modalCreateUserPassword').value = '';
            document.getElementById('modalCreateUserPassword').placeholder = DbData[4][i].password;
            
            $('select[name=modalCreateUserSelectPosition]').val(DbData[4][i].position);
            $('.modalCreateUserSelectPosition').selectpicker('refresh');
            
            break;
        }
    }
    

    $("#modalCreateUser").modal("show");

    document.getElementById('updateUser').addEventListener('click', updateUser);
    document.getElementById('rmUser').addEventListener('click', rmUser);
}
//--------------------------------------------------------------------------------------
// Creat row on table
var idSelectEdit;
function buildDataModalCreate(){
    if(isOpenedProposalSheetDetail){
        document.getElementById('modalCreateDetail').children[0].children[0].children[0].innerText = "Thêm một văn phòng phẩm";
        $("#modalCreateDetail").modal("show");
    }
    else if(tabSelect == 1){
        var d = new Date();
        var n = d.getMonth();
        document.getElementById('modalRegisterCreate').children[0].children[0].children[0].innerText = "Tạo phiếu đề xuất tháng " + ( n + 1);

        $("#modalRegisterCreate").modal("show");
    }
    else if(tabSelect == 5){
        buildDataModalCreateUser();
        $("#modalCreateUser").modal("show");
    }
    else{
        if(tabSelect == 2) document.getElementById('modalCreate').children[0].children[0].children[0].innerText = "Tạo mới 1 vpp";
        else if(tabSelect == 3) document.getElementById('modalCreate').children[0].children[0].children[0].innerText = "Tạo mới 1 đơn vị";
        else if(tabSelect == 4) document.getElementById('modalCreate').children[0].children[0].children[0].innerText = "Tạo mới 1 phòng";

        $("#modalCreate").modal("show");
    }
}
function cr(){
    if(document.getElementById('crname').value == "") return;

    urlStr = "";
    if(tabSelect == 2) urlStr = "/crEquipment";
    else if(tabSelect == 3) urlStr = "/crUnit";
    else if(tabSelect == 4) urlStr = "/crDepartment";

    dataSend = null;
    if(tabSelect == 1){
        //document.getElementById("departmentList").selectedIndex
        //document.getElementById("departmentList").value

        dataSend = {'name': document.getElementById('crname').value};
    }
    else{
        dataSend = {'name': document.getElementById('crname').value};
    }
    
    $.ajax({
        type: 'POST',
        //contentType: "application/json; charset=utf-8",
        dataType: 'json',
        url: urlStr,
        data: JSON.stringify(dataSend),
        success: function (msg) {
            DbData[tabSelect - 1] = msg;
            buildTableEquipmentOrUnit();
            updateDepartmentOnCreateAndEditModal();
            updateEquipmentOnCreateAndEditModal();
            updateUnitOnCreateAndEditModal();
            console.log(DbData[tabSelect - 1]);
        },
        error: function(msg){
            console.warn(msg);
        }
    });
}
function updateDepartmentOnCreateAndEditModal(){
    document.getElementById("departmentList").innerHTML = "";
    for(let i = 0; i < DbData[3].length; i++){
        //document.getElementById("departmentList").innerHTML += "<option>" + DbData[3][i].name + "</option>";
        $("#departmentList").append($('<option>', {value: i,text:  DbData[3][i].name}));
    }
    $('.selectpicker').selectpicker('refresh');
}
function updateEquipmentOnCreateAndEditModal(){
    document.getElementById("equipmentList").innerHTML = "";
    // On edit modal - detail proposal of sheet
    document.getElementById("modalEditDetailequipmentList").innerHTML = "";
    for(let i = 0; i < DbData[1].length; i++){
        //document.getElementById("departmentList").innerHTML += "<option>" + DbData[3][i].name + "</option>";
        $("#equipmentList").append($('<option>', {value: i,text:  DbData[1][i].name}));
        $("#modalEditDetailequipmentList").append($('<option>', {value: i,text:  DbData[1][i].name}));
    }
    $('.equipmentListSelectpicker').selectpicker('refresh');
    $('.modalEditDetailequipmentListSelectpicker').selectpicker('refresh');
}
function updateUnitOnCreateAndEditModal(){
    document.getElementById("unitList").innerHTML = "";
    document.getElementById("modalEditDetailunitList").innerHTML = "";
    for(let i = 0; i < DbData[2].length; i++){
        //document.getElementById("departmentList").innerHTML += "<option>" + DbData[3][i].name + "</option>";
        $("#unitList").append($('<option>', {value: i,text:  DbData[2][i].name}));
        $("#modalEditDetailunitList").append($('<option>', {value: i,text:  DbData[2][i].name}));
    }
    $('.unitListSelectpicker').selectpicker('refresh');
    $('.modalEditDetailListSelectpicker').selectpicker('refresh');
}
function updatePositionOnCreateAndEditUserModal(){
    document.getElementById("modalCreateUserSelectPosition").innerHTML = "";
    
    $("#modalCreateUserSelectPosition").append($('<option>', {value: 0,text:  "ADMIN"}));
    $("#modalCreateUserSelectPosition").append($('<option>', {value: 1,text:  "STAFF"}));

    $('.modalCreateUserSelectPosition').selectpicker('refresh');
}
// edit-delete row on table
function buildDataModalEdit(id){
    if(!isOpenedProposalSheetDetail){
        idSelectEdit = id;
        modalContentEdit = document.getElementById('modalContentEdit');

        document.getElementById('modalEdit').children[0].children[0].children[0].innerText = "Chỉnh sửa";
        
        // Find name with id
        for(let i = 0; i < DbData[tabSelect - 1].length; i++){
            if(DbData[tabSelect - 1][i].id == id){
                modalContentEdit.children[1].children[0].placeholder = DbData[tabSelect - 1][i].name;
                break;
            }
        }
    }
    else{
        idSelectEdit = id;
        modalContentEdit = document.getElementById('modalEditDetailContent');

        document.getElementById('modalEditDetail').children[0].children[0].children[0].innerText = "Chỉnh sửa";
        // Find name with id
        for(let i = 0; i < detailData.length; i++){
            if(detailData[i].id == id){
                // Find equipment name
                for(let j = 0; j < DbData[1].length; j++){
                    if(DbData[1][j].id == detailData[i].idEquipment){
                        //modalContentEdit.children[1].children[0].value(j);
                        //modalContentEdit.children[1].children[0].children[0].children[1].innerText = DbData[1][j].name;
                        $('select[name=modalEditDetailequipmentSelect]').val(j);
                        $('.modalEditDetailequipmentListSelectpicker').selectpicker('refresh');
                        break;
                    }
                }
                // Find unit name
                for(let j = 0; j < DbData[2].length; j++){
                    if(DbData[2][j].id == detailData[i].idUnit){
                        //modalContentEdit.children[3].children[0].value(j);
                        //modalContentEdit.children[3].children[0].children[0].children[1].innerText = DbData[2][j].name;
                        $('select[name=modalEditDetailunitSelect]').val(j);
                        $('.modalEditDetailunitListSelectpicker').selectpicker('refresh');
                        break;
                    }
                }
                modalContentEdit.children[5].children[0].placeholder = detailData[i].quantity;
                
                //$('select[name=equipmentSelect]').text("Mì gói");
                //$("#equipmentList > option[value=2]").text("Ádf");
                
                break;
            }
        }
        $("#modalEditDetail").modal("show");
    }
}
function update(){
    if(document.getElementById('name').value == "") return;

    urlStr = "";
    if(tabSelect == 2) urlStr = "/updateEquipment";
    else if(tabSelect == 3) urlStr = "/updateUnit";
    else if(tabSelect == 4) urlStr = "/updateDepartment";
    $.ajax({
        type: 'POST',
        //contentType: "application/json; charset=utf-8",
        dataType: 'json',
        url: urlStr,
        data: JSON.stringify({
            'id': idSelectEdit,
            'name': document.getElementById('name').value,
        }),
        success: function (msg) {
            //document.getElementById('row' + idSelectEdit).children[1].innerText = document.getElementById('name').value;
            DbData[tabSelect - 1] = msg;
            buildTableEquipmentOrUnit();
            updateDepartmentOnCreateAndEditModal();
            updateEquipmentOnCreateAndEditModal();
            updateUnitOnCreateAndEditModal();
        },
        error: function(msg){
            console.warn(msg);
        }
    });
}
function rmOnEdit(){
    urlStr = "";
    if(tabSelect == 2) urlStr = "/rmEquipment";
    else if(tabSelect == 3) urlStr = "/rmUnit";
    else if(tabSelect == 4) urlStr = "/rmDepartment";
    $.ajax({
        type: 'POST',
        //contentType: "application/json; charset=utf-8",
        dataType: 'json',
        url: urlStr,
        data: JSON.stringify({
            'id': idSelectEdit,
            'name': document.getElementById('name').value,
        }),
        success: function (msg) {
            DbData[tabSelect - 1] = msg;
            buildTableEquipmentOrUnit();
            updateDepartmentOnCreateAndEditModal();
            updateEquipmentOnCreateAndEditModal();
            updateUnitOnCreateAndEditModal();
        },
        error: function(msg){
            console.warn(msg);
        }
    });
}
//--------------------------------------------------------------------------------------
// Build table common
function buildTableEquipmentOrUnit(){
    if(tabSelect == 1){
        //a = document.getElementById('table');
        //a.innerHTML = "<thead><tr><th>#</th><th>Phòng</th><th>Thời gian đăng ký</th><th>Người đề xuất</th><th>Trạng thái</th><th>Hành động</th></tr></thead>";
        h = document.getElementById('headerTable');
        h.innerHTML = "<tr><th>ID</th><th>Phòng</th><th>Thời gian đăng ký</th><th>Người đề xuất</th><th>Trạng thái</th><th>Hành động</th></tr>";

        var Parent = document.getElementById('bodyContent');
        while(Parent.hasChildNodes()){
            Parent.removeChild(Parent.firstChild);
        }
        //for(let i = 0; i < DbData[tabSelect - 1].length; i++){
        //    a.innerHTML += buildRowProposalSheet(DbData[tabSelect - 1][i]);
        //}
        //a.innerHTML += "</tbody>";


        for(let i = 0; i < DbData[tabSelect - 1].length; i++){
            buildRowProposalSheet(DbData[tabSelect - 1][i]);                
        }
        
        
        return;
    }
    //a = document.getElementById('table');
    //a.innerHTML = "<thead><tr><th>ID</th><th>Tên</th><th>Hành động</th></tr></thead><tbody>";
    //for(let i = 0; i < DbData[tabSelect - 1].length; i++){
    //    a.innerHTML += buildRowEquipmentOrUnit(DbData[tabSelect - 1][i]);
    //}
    //a.innerHTML += "</tbody>";

    h = document.getElementById('headerTable');
    h.innerHTML = "<thead><tr><th>ID</th><th>Tên</th><th>Hành động</th></tr></thead><tbody>";

    var Parent = document.getElementById('bodyContent');
    while(Parent.hasChildNodes()){
        Parent.removeChild(Parent.firstChild);
    }
    for(let i = 0; i < DbData[tabSelect - 1].length; i++){
        buildRowEquipmentOrUnit(DbData[tabSelect - 1][i]);                
    }
}
// Build row common
function buildRowProposalSheet(obj){
    oTime = new Date(obj.monthYear*1000);
    departmentStr = "";
    for(let i = 0; i < DbData[3].length; i++){
        if(DbData[3][i].id == obj.idDepartment){
            departmentStr = DbData[3][i].name;
            break;
        }
    }
    userNameStr = "";
    for(let i = 0; i < DbData[4].length; i++){
        if(DbData[4][i].id == obj.idUser){
            userNameStr = DbData[4][i].name;
            break;
        }
    }
    badgesStr = "";
    statusStr = "";
    if(obj.status == 0){
        statusStr = "Chờ xác nhận";
        badgesStr = "warning";
    }
    else if(obj.status == 1){
        statusStr = "Đã xác nhận";
        badgesStr = "success";
    }
    else if(obj.status == 2){
        statusStr = "Hủy";
        badgesStr = "danger";
    }

    a = document.getElementById('bodyContent');
    var row = a.insertRow(a.rows.length);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);
    var cell5 = row.insertCell(4);
    var cell6 = row.insertCell(5);
    cell1.innerHTML = "<td>" + obj.id + "</td>";
    cell2.innerHTML = "<td>" + departmentStr + "</td>";
    cell3.innerHTML = "<td>" + (oTime.getMonth() + 1) + "/" + (oTime.getYear() + 1900) + "</td>";
    cell4.innerHTML = "<td>" + userNameStr +"</td>";
    cell5.innerHTML = '<div class="btn-group"><button type="button" class="btn btn-' + badgesStr + ' dropdown-toggle btn-mod" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' + statusStr+ '</button><div class="dropdown-menu"><a class="dropdown-item content-font-size cursor-pointer" onclick=\"proposalSheetChangeStatus(0,' + obj.id + ')\">Chờ xác nhận</a><a class="dropdown-item content-font-size cursor-pointer" onclick=\"proposalSheetChangeStatus(1,' + obj.id + ')\">Xác nhận</a><a class="dropdown-item content-font-size cursor-pointer" onclick=\"proposalSheetChangeStatus(2,' + obj.id + ')\">Hủy</a></div></div>';
    cell6.innerHTML = "<td><div class=\"d-flex flex-row\"><a onclick=\"openListRegister(" + obj.id + ")\" class=\"settings\" title=\"Chỉnh sửa\" data-toggle=\"tooltip\"><i class=\"fa fa-edit text-primary fa-fw fa-2x\"></i></a><a onclick=\"rmRegister(" + obj.id + ")\" class=\"delete\" title=\"Hủy\" data-toggle=\"tooltip\"><i class=\"fa fa-trash fa-fw fa-2x\"></i></a></div></td>";
}
function buildRowEquipmentOrUnit(obj){
    //return "<tr id=\"row" + obj.id + "\"><td>" + obj.id + "</td><td>" + obj.name + "</td><td><div class=\"d-flex flex-row\"><a href=\"#\" class=\"settings\" title=\"Chỉnh sửa\" data-toggle=\"modal\" data-target=\"#modalEdit\" onclick=\"buildDataModalEdit(" + obj.id + ")\"><i class=\"fa fa-edit text-primary fa-fw fa-2x\"></i></a><a href=\"#\" class=\"delete\" title=\"Xóa\" data-toggle=\"tooltip\"><i class=\"fa fa-trash fa-fw fa-2x\"></i></a></div></td></tr>";
    a = document.getElementById('bodyContent');
    var row = a.insertRow(a.rows.length);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);

    cell1.innerHTML = "<td>" + obj.id + "</td>";
    cell2.innerHTML = "<td>" + obj.name + "</td>";
    cell3.innerHTML = "<td><div class=\"d-flex flex-row\"><a href=\"#\" class=\"settings\" title=\"Chỉnh sửa\" data-toggle=\"modal\" data-target=\"#modalEdit\" onclick=\"buildDataModalEdit(" + obj.id + ")\"><i class=\"fa fa-edit text-primary fa-fw fa-2x\"></i></a><a href=\"#\" class=\"delete\" title=\"Xóa\" data-toggle=\"tooltip\"><i class=\"fa fa-trash fa-fw fa-2x\"></i></a></div></td>";
}
// Build table user
function buildTableUser(){
    h = document.getElementById('headerTable');
    h.innerHTML = "<tr><th>ID</th><th>Tên</th><th>Email</th><th>Mật khẩu</th><th>Chức vụ</th><th></th></tr>";

    var Parent = document.getElementById('bodyContent');
    while(Parent.hasChildNodes()){
        Parent.removeChild(Parent.firstChild);
    }
    //for(let i = 0; i < DbData[tabSelect - 1].length; i++){
    //    a.innerHTML += buildRowProposalSheet(DbData[tabSelect - 1][i]);
    //}
    //a.innerHTML += "</tbody>";


    for(let i = 0; i < DbData[tabSelect - 1].length; i++){
        buildRowTableUser(DbData[tabSelect - 1][i]);                
    }
}
function buildRowTableUser(obj){
    if(obj.position == 0){
        positionStr = "ADMIN";
        badgesStr = "badge-warning";
    }
    else{
        positionStr = "STAFF";
        badgesStr = "badge-primary";
    }
    

    a = document.getElementById('bodyContent');
    var row = a.insertRow(a.rows.length);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);
    var cell5 = row.insertCell(4);
    var cell6 = row.insertCell(5);
    cell1.innerHTML = "<td>" + obj.id + "</td>";
    cell2.innerHTML = "<td>" + obj.name + "</td>";
    cell3.innerHTML = "<td>" + obj.email + "</td>";
    cell4.innerHTML = "<td>" + obj.password +"</td>";
    cell5.innerHTML = "<td><span class=\"badge " + badgesStr + "\" style=\"font-size: 12px;\">" + positionStr + "</span></td>";
    cell6.innerHTML = "<td><div class=\"d-flex flex-row\"><a onclick=\"buildDataModalUpdateUser(" + obj.id + ")\" class=\"settings\" title=\"Chỉnh sửa\" data-toggle=\"tooltip\"><i class=\"fa fa-edit text-primary fa-fw fa-2x\"></i></a><a href=\"#\" class=\"delete\" title=\"Hủy\" data-toggle=\"tooltip\"><i class=\"fa fa-trash fa-fw fa-2x\"></i></a></div></td>";
}
// Build table proposal sheet detail
function buildTableProposalSheetDetail(data){
    //a = document.getElementById('table');
    //a.innerHTML = "<thead><tr><th>#</th><th>Phòng</th><th>Thời gian đăng ký</th><th>Người đề xuất</th><th>Trạng thái</th><th>Hành động</th></tr></thead>";
    h = document.getElementById('headerTable');
    h.innerHTML = "<tr><th>ID</th><th>Tên vpp</th><th>Đơn vị</th><th>Số lượng</th><th>Được tạo bởi</th><th>Hành động</th></tr>";

    var Parent = document.getElementById('bodyContent');
    while(Parent.hasChildNodes()){
        Parent.removeChild(Parent.firstChild);
    }
    //for(let i = 0; i < DbData[tabSelect - 1].length; i++){
    //    a.innerHTML += buildRowProposalSheet(DbData[tabSelect - 1][i]);
    //}
    //a.innerHTML += "</tbody>";


    for(let i = 0; i < data.length; i++){
        buildRowProposalSheetDetail(data[i]);                
    }
}
function buildRowProposalSheetDetail(obj){
    departmentStr = "";
    for(let i = 0; i < DbData[3].length; i++){
        if(DbData[3][i].id == obj.idDepartment){
            departmentStr = DbData[3][i].name;
            break;
        }
    }
    userNameStr = "";
    for(let i = 0; i < DbData[4].length; i++){
        if(DbData[4][i].id == obj.idUserCreate){
            userNameStr = DbData[4][i].name;
            break;
        }
    }
    equipmentStr = "";
    for(let i = 0; i < DbData[1].length; i++){
        if(DbData[1][i].id == obj.idEquipment){
            equipmentStr = DbData[1][i].name;
            break;
        }
    }
    unitStr = "";
    for(let i = 0; i < DbData[2].length; i++){
        if(DbData[2][i].id == obj.idUnit){
            unitStr = DbData[2][i].name;
            break;
        }
    }

    a = document.getElementById('bodyContent');
    var row = a.insertRow(a.rows.length);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);
    var cell5 = row.insertCell(4);
    var cell6 = row.insertCell(5);
    var cell7 = row.insertCell(6);
    cell1.innerHTML = "<td>" + obj.id + "</td>";
    cell2.innerHTML = "<td>" + equipmentStr + "</td>";
    cell3.innerHTML = "<td>" + unitStr + "</td>";
    cell4.innerHTML = "<td>" + obj.quantity + "</td>";
    cell5.innerHTML = "<td>" + userNameStr +"</td>";
    cell6.innerHTML = "<td><div class=\"d-flex flex-row\"><a onclick=\"buildDataModalEdit(" + obj.id + ")\" class=\"settings\" title=\"Chỉnh sửa\" data-toggle=\"tooltip\"><i class=\"fa fa-edit text-primary fa-fw fa-2x\"></i></a><a href=\"#\" class=\"delete\" title=\"Hủy\" data-toggle=\"tooltip\"><i class=\"fa fa-trash fa-fw fa-2x\"></i></a></div></td>";
}
//--------------------------------------------------------------------------------------
function proposalSheetChangeStatus(status, id){
    updateRegister(id,status);
}
//--------------------------------------------------------------------------------------
// Navigation
var tabSelect = 1, isOpenedProposalSheetDetail = false, proposalSheetIndexOpenned;
function selectTab(tabIndex){
    t1.classList.remove('nav-selected');
    t2.classList.remove('nav-selected');
    t3.classList.remove('nav-selected');
    t4.classList.remove('nav-selected');
    t5.classList.remove('nav-selected');
    t6.classList.remove('nav-selected');
    window['t' + tabIndex].classList.add('nav-selected');
    
    tabSelect = tabIndex;
    strTitle = '<h2 style="cursor: pointer;">'
    
    if(tabIndex == 1){
        document.getElementById('lineTitle').innerHTML = strTitle + 'Danh sách đề xuất</h2>';
    }
    else if(tabIndex == 2){
        document.getElementById('lineTitle').innerHTML = strTitle + 'Danh mục thiết bị - dụng cụ văn phòng phẩm</h2>';
    }
    else if(tabIndex == 3){
        document.getElementById('lineTitle').innerHTML = strTitle + 'Danh sách đơn vị</h2>';
    }
    else if(tabIndex == 4){
        document.getElementById('lineTitle').innerHTML = strTitle + 'Danh sách các phòng ban</h2>';
    }
    else if(tabIndex == 5){
        document.getElementById('lineTitle').innerHTML = strTitle + 'Danh sách nhân viên</h2>';
    }
    else if(tabIndex == 5){
        document.getElementById('lineTitle').innerHTML = strTitle + 'Danh sách nhân viên</h2>';
    }
    // Clear flag is opened proposal sheet detail
    isOpenedProposalSheetDetail = false;

    if(tabIndex < 5){
        buildTableEquipmentOrUnit();
    }
    else if(tabIndex == 5){
        buildTableUser();
    }
    else if(tabIndex == 6){
        
    }
}
function openListRegister(idProposalSheet){
    if(isOpenedProposalSheetDetail) return;
    isOpenedProposalSheetDetail = true;
    proposalSheetIndexOpenned = idProposalSheet;

    // Get month year string and id department
    indexOnArray = 0;
    monthYearStrOnListDetailOpened = ""
    departmentName = "";
    for(let i = 0; i < DbData[0].length; i++){
        if(idProposalSheet == DbData[0][i].id){
            oTime = new Date(DbData[0][i].monthYear*1000);
            monthYearStrOnListDetailOpened = (oTime.getMonth() + 1) + "/" + (oTime.getYear() + 1900);

            for(let j = 0; j < DbData[3].length; j++){
                if(DbData[0][i].idDepartment == DbData[3][j].id){
                    departmentName = DbData[3][j].name;
                    break;
                }
            }
            indexOnArray = i;
            break;
        }
    }
    $.ajax({
        type: 'POST',
        contentType: "application/json",
        dataType: 'json',
        url: '/getProposalSheetDetail',
        data: JSON.stringify({
            'idProposalSheet': proposalSheetIndexOpenned
        }),
        success: function (msg) {
            console.log(msg);
            var h = document.createElement("H2");
            // Get department name from id
                            
            var t = document.createTextNode("/ Phiếu đề xuất phòng " + departmentName + " - " + monthYearStrOnListDetailOpened);
            h.appendChild(t);
            document.getElementById('lineTitle').appendChild(h);

            buildTableProposalSheetDetail(msg);
            detailData = msg;
        },
        error: function(msg){
            console.log(msg);
        }
    });
}
//--------------------------------------------------------------------------------------
// Cookie
function getCookie(name) {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) return parts.pop().split(';').shift();
}
//--------------------------------------------------------------------------------------
var DbData, detailData;
function saveAs(blob, fileName) {
    var url = window.URL.createObjectURL(blob);

    var anchorElem = document.createElement("a");
    anchorElem.style = "display: none";
    anchorElem.href = url;
    anchorElem.download = fileName;

    document.body.appendChild(anchorElem);
    anchorElem.click();

    document.body.removeChild(anchorElem);

    // On Edge, revokeObjectURL should be called only after
    // a.click() has completed, atleast on EdgeHTML 15.15048
    setTimeout(function() {
        window.URL.revokeObjectURL(url);
    }, 1000);
}
$(document).ready(function () {
    $.ajax({
        type: 'POST',
        contentType: "application/json",
        dataType: 'json',
        url: '/getData',
        data: JSON.stringify({
            'data': 'null'
        }),
        success: function (msg) {
            //alert('wow' + msg);
            //console.log(msg);
            DbData = msg;
            updateDepartmentOnCreateAndEditModal();
            updateEquipmentOnCreateAndEditModal();
            updateUnitOnCreateAndEditModal();
            updatePositionOnCreateAndEditUserModal();
            
            hiObj = document.getElementById("hi");
            if(hiObj.innerText != ("Hi " + DbData[5].name)){
                document.getElementById("hi").innerText = "Hi " + DbData[5].name;
            }

            buildTableEquipmentOrUnit();
        },
        error: function(msg){
            console.log(msg);
        }
    });

    $('[data-toggle="tooltip"]').tooltip();
    document.getElementById('logoutBtn').addEventListener('click', logout);

    var t1 = document.getElementById('t1');
    var t2 = document.getElementById('t2');
    var t3 = document.getElementById('t3');
    var t4 = document.getElementById('t4');
    var t5 = document.getElementById('t5');
    var t6 = document.getElementById('t6');
    t1.addEventListener('click', function(){selectTab(1)});
    t2.addEventListener('click', function(){selectTab(2)});
    t3.addEventListener('click', function(){selectTab(3)});
    t4.addEventListener('click', function(){selectTab(4)});
    t5.addEventListener('click', function(){selectTab(5)});
    t6.addEventListener('click', function(){selectTab(6)});

    document.getElementById('create').addEventListener('click', cr);
    document.getElementById('update').addEventListener('click', update);
    document.getElementById('delete').addEventListener('click', rmOnEdit);

    document.getElementById('createRegister').addEventListener('click', crRegister);
    // Proposal detail
    document.getElementById('createProposalDetail').addEventListener('click', crProposalDetail);
    document.getElementById('modalEditDetailUpdate').addEventListener('click', updateProposalDetail);
    document.getElementById('modalEditDetailDelete').addEventListener('click', rmProposalDetail);

    document.getElementById('createUser').addEventListener('click', crUser);
    
    document.getElementById("hi").innerText = "Hi " + getCookie('userName');

    // saving text file
    //var blob2 = new Blob(['<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?><Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40"> <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">  <Author>Thien Dong</Author>  <LastAuthor>Thien Dong</LastAuthor>  <Created>2020-11-28T11:11:16Z</Created>  <LastSaved>2020-11-28T11:11:59Z</LastSaved>  <Version>14.00</Version> </DocumentProperties> <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">  <AllowPNG/> </OfficeDocumentSettings> <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">  <WindowHeight>12600</WindowHeight>  <WindowWidth>27795</WindowWidth>  <WindowTopX>480</WindowTopX>  <WindowTopY>105</WindowTopY>  <ProtectStructure>False</ProtectStructure>  <ProtectWindows>False</ProtectWindows> </ExcelWorkbook> <Styles>  <Style ss:ID="Default" ss:Name="Normal">   <Alignment ss:Vertical="Bottom"/>   <Borders/>   <Font ss:FontName="Calibri" x:CharSet="163" x:Family="Swiss" ss:Size="11"    ss:Color="#000000"/>   <Interior/>   <NumberFormat/>   <Protection/>  </Style>  <Style ss:ID="s64">   <Borders>    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>   </Borders>  </Style>  <Style ss:ID="s65">   <Borders>    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>   </Borders>   <Font ss:FontName="Calibri" x:CharSet="163" x:Family="Swiss" ss:Size="26"    ss:Color="#FF0000"/>  </Style>  <Style ss:ID="s66">   <Borders>    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>   </Borders>   <Font ss:FontName="Calibri" x:CharSet="163" x:Family="Swiss" ss:Size="12"    ss:Color="#99CCFF"/>  </Style> </Styles> <Worksheet ss:Name="Sheet1">  <Table ss:ExpandedColumnCount="6" ss:ExpandedRowCount="12" x:FullColumns="1"   x:FullRows="1" ss:DefaultRowHeight="15">   <Row ss:AutoFitHeight="0"/>   <Row ss:Index="4" ss:AutoFitHeight="0" ss:Height="33.75">    <Cell ss:Index="2" ss:StyleID="s64"/>    <Cell ss:StyleID="s65"><Data ss:Type="String">adf</Data></Cell>    <Cell ss:StyleID="s64"/>    <Cell ss:StyleID="s64"/>    <Cell ss:StyleID="s64"/>   </Row>   <Row ss:AutoFitHeight="0">    <Cell ss:Index="2" ss:StyleID="s64"/>    <Cell ss:StyleID="s64"/>    <Cell ss:StyleID="s64"/>    <Cell ss:StyleID="s64"/>    <Cell ss:StyleID="s64"/>   </Row>   <Row ss:AutoFitHeight="0" ss:Height="15.75">    <Cell ss:Index="2" ss:StyleID="s64"/>    <Cell ss:StyleID="s64"/>    <Cell ss:StyleID="s64"/>    <Cell ss:StyleID="s66"><Data ss:Type="String">adfasdf</Data></Cell>    <Cell ss:StyleID="s64"/>   </Row>   <Row ss:AutoFitHeight="0">    <Cell ss:Index="2" ss:StyleID="s64"/>    <Cell ss:StyleID="s64"/>    <Cell ss:StyleID="s64"/>    <Cell ss:StyleID="s64"/>    <Cell ss:StyleID="s64"/>   </Row>   <Row ss:AutoFitHeight="0">    <Cell ss:Index="2" ss:StyleID="s64"/>    <Cell ss:StyleID="s64"/>    <Cell ss:StyleID="s64"/>    <Cell ss:StyleID="s64"/>    <Cell ss:StyleID="s64"/>   </Row>   <Row ss:AutoFitHeight="0">    <Cell ss:Index="2" ss:StyleID="s64"/>    <Cell ss:StyleID="s64"/>    <Cell ss:StyleID="s64"/>    <Cell ss:StyleID="s64"/>    <Cell ss:StyleID="s64"/>   </Row>   <Row ss:AutoFitHeight="0">    <Cell ss:Index="2" ss:StyleID="s64"/>    <Cell ss:StyleID="s64"/>    <Cell ss:StyleID="s64"/>    <Cell ss:StyleID="s64"/>    <Cell ss:StyleID="s64"/>   </Row>   <Row ss:AutoFitHeight="0">    <Cell ss:Index="2" ss:StyleID="s64"/>    <Cell ss:StyleID="s64"/>    <Cell ss:StyleID="s64"/>    <Cell ss:StyleID="s64"/>    <Cell ss:StyleID="s64"/>   </Row>   <Row ss:AutoFitHeight="0">    <Cell ss:Index="2" ss:StyleID="s64"/>    <Cell ss:StyleID="s64"/>    <Cell ss:StyleID="s64"/>    <Cell ss:StyleID="s64"/>    <Cell ss:StyleID="s64"/>   </Row>  </Table>  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">   <PageSetup>    <Header x:Margin="0.3"/>    <Footer x:Margin="0.3"/>    <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>   </PageSetup>   <Unsynced/>   <Print>    <ValidPrinterInfo/>    <HorizontalResolution>600</HorizontalResolution>    <VerticalResolution>600</VerticalResolution>   </Print>   <Selected/>   <Panes>    <Pane>     <Number>3</Number>     <ActiveRow>10</ActiveRow>     <ActiveCol>7</ActiveCol>    </Pane>   </Panes>   <ProtectObjects>False</ProtectObjects>   <ProtectScenarios>False</ProtectScenarios>  </WorksheetOptions> </Worksheet> <Worksheet ss:Name="Sheet2">  <Table ss:ExpandedColumnCount="1" ss:ExpandedRowCount="1" x:FullColumns="1"   x:FullRows="1" ss:DefaultRowHeight="15">   <Row ss:AutoFitHeight="0"/>  </Table>  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">   <PageSetup>    <Header x:Margin="0.3"/>    <Footer x:Margin="0.3"/>    <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>   </PageSetup>   <Unsynced/>   <ProtectObjects>False</ProtectObjects>   <ProtectScenarios>False</ProtectScenarios>  </WorksheetOptions> </Worksheet> <Worksheet ss:Name="Sheet3">  <Table ss:ExpandedColumnCount="1" ss:ExpandedRowCount="1" x:FullColumns="1"   x:FullRows="1" ss:DefaultRowHeight="15">   <Row ss:AutoFitHeight="0"/>  </Table>  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">   <PageSetup>    <Header x:Margin="0.3"/>    <Footer x:Margin="0.3"/>    <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>   </PageSetup>   <Unsynced/>   <ProtectObjects>False</ProtectObjects>   <ProtectScenarios>False</ProtectScenarios>  </WorksheetOptions> </Worksheet></Workbook>'], {type: "text/plain"});
    //var fileName2 = "cool.xml";
    //saveAs(blob2, fileName2);
});